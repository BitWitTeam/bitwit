package com.example.bitwit;
import java.util.LinkedList;
import java.util.HashMap;
public class ParseHTML
{
	ParseHTML(String S)
	{
		repeat_letter=false;
		letter_to_repeat='\0';
		sentence=S;
		sentence_place=0;
		// used to associate contents
		database=new HashMap<String,String>();
		build();
	}
	private boolean repeat_letter;
	private char letter_to_repeat;
	private String sentence;
	private int sentence_place;
	private char take()
	{
		char c;
		if(repeat_letter)
		{
			repeat_letter=false;
			return letter_to_repeat;
		}
		c=sentence.charAt(sentence_place);
		sentence_place++;
		return c;
	}
	private void give(char C)
	{
		repeat_letter=true;
		letter_to_repeat=C;
	}
	private void criticalError()
	{
		throw new RuntimeException("PROBLEM!");
	}
	private String buildSymbolName()
	{
		char c;
		StringBuilder sb=new StringBuilder();
		c=take();
		if(TextUtility.isLetter(c))
		{
			do
			{
				sb.append(c);
				c=take();
			}
			// assuming tag names must be letter than any combination
			while(
				TextUtility.isLetter(c)
				||
				TextUtility.isNumber(c)
			);
			give(c);
		}
		else criticalError();
		return sb.toString();
	}
	private void cycleBlanks()
	// won't cause an error, only end quietly
	{
		char c;
		do c=take(); while(TextUtility.isWhitespace(c));
		give(c);
	}
	private boolean checkSymbolName()
	{
		char c;
		c=take();
		give(c);
		if(TextUtility.isLetter(c))
		{
			buildSymbolName();
			return true;
		}
		else return false;
	}
	private void ensureCharacter(char Q)
	{
		if(take()!=Q)
			criticalError();
	}
	private void cycleQuotedString()
	{
		char c;
		c=take();
		if(c=='\''||c=='\"')
		{
			char quote;
			quote=c;
			do
			{
				c=take();
			}
			while(c!=quote);
		}
		else criticalError();
	}
	private boolean checkQuotedString()
	{
		char c;
		c=take();
		if(c=='\''||c=='\"')
		{
			char quote;
			quote=c;
			do
			{
				c=take();
			}
			while(c!=quote);
			return true;
		}
		else
		{
			give(c);
			return false;
		}
	}
	private void cycleHeader()
	{
		char c;
		c=take();
		if(c=='<')
		{
			c=take();
			if(c=='!')
			{
				String header;
				header=buildSymbolName();
				if(header.equalsIgnoreCase("DOCTYPE"))
				{
					for(;;)
					{
						boolean is_name,is_quoted;
						cycleBlanks();
						is_name=checkSymbolName();
						is_quoted=checkQuotedString();
						// now will work on all authorings
						if((!is_name)&&(!is_quoted))
							break;
					}
					cycleBlanks(); // just in case
					if(take()!='>')
						criticalError();
				}
				else criticalError();
			}
			else criticalError();
		}
		else criticalError();
	}
	private HashMap<String,String> database;
	private void build()
	{
		char c;
		final int OPENING=0;
		final int CLOSING=1;
		final int ALONE=2;
		int kind;
		boolean inside_script=false;
		boolean ending_script_tag=false;
		boolean ending_tag=false;
		StringBuilder contents=null;
		// used to build the path while descending
		LinkedList<String> path=new LinkedList<String>();
		String last_tag="";
		// the last tag to know which are files
		HashMap<String,Integer> same_name_counts=new HashMap<String,Integer>();
		cycleHeader(); // gets past the mandatory HTML header
		do
		{
			c=take();
			if(c=='<'&&inside_script)
			{
				char letter;
				letter=take();
				if(letter=='/')
				{
					// allowed to get in
					ending_script_tag=true;
					inside_script=false;
				}
				// no matter what allow the letter to go back
				give(letter);
			}
			if(c=='<'&&(!inside_script))
			{
				String name;
				char letter;
				letter=take();
				if(letter=='/')
				{
					// closing of a directory
					ending_tag=true;
					kind=CLOSING;
				}
				else
				// not a closing
				{
					kind=OPENING;
					give(letter);
				}
				name=buildSymbolName();
				// inside the name need to get out of it
				//  can't just go to next '>' since the quoted strings
				if(
					name.compareTo("script")==0
					&&
					(!ending_script_tag)
				)
				{
					inside_script=true;
				}
				if(ending_tag)
				// special case for a script exit
				{
					ending_script_tag=false;
					ending_tag=false;
				}
				for(;;)
				{
					cycleBlanks();
					if(!checkSymbolName())
						break;
					ensureCharacter('=');
					cycleQuotedString();
				}
				c=take();
				if(c=='>')
				{
					// openning of a directory
				}
				else
				{
					if(c=='/')
					{
						c=take();
						if(c=='>')
						{
							// a single entry
							kind=ALONE;
						}
						else criticalError();
					}
					else criticalError();
				}
				switch(kind)
				{
				case OPENING:
				//	System.out.print("OPEN ");
					path.addLast(name);
					contents=new StringBuilder();
					break;
				case CLOSING:
				//	System.out.print("CLOSE ");
					break;
				case ALONE:
				//	System.out.print("LONE ");
					// has no contents
					break;
				}
				StringBuilder real_path_maker=new StringBuilder();
				int place;
				for(place=0;place<path.size();place++)
				{
					real_path_maker.append('/');
					real_path_maker.append(path.get(place));
				}
				String real_path=real_path_maker.toString();
			//	System.out.println(real_path);
				switch(kind)
				{
				case OPENING:
					last_tag=name;
					break;
				case CLOSING:
					if(contents!=null)
					{
						if(!real_path.endsWith("script"))
						// I really don't care about the javascript
						{
							database.put(real_path,contents.toString());
						}
						contents=null;
					}
					path.removeLast(); // remove once have used it
					/*
					if(last_tag.compareTo(name)!=0)
					{
						database.remove(real_path);
					}
					*/
					break;
				}
			}
			else
			{
				if(contents!=null)
					contents.append(c);
			}
		}
		while(sentence_place<sentence.length());
		/*
		// listing contents of hash
		for(String key:database.keySet())
		{
			System.out.println(key);
			System.out.println("BEGIN");
			System.out.print(database.get(key));
			System.out.println("\nEND");
		}
		*/
	}
	public String acquire(String path)
	{
		return database.get(path);
	}
}

package com.example.bitwit;

import java.util.*;

class ParseJSON
{
	private int place; // the place in the word
	String word; // input actions
	private void resetAutomata(String closed_stream)
	{
		place=0;
		word=closed_stream;
		// used by take( and give(
		repeated=false;
		// used in quotedString(
		qs_word="";
	}
	private boolean repeated;
	private char letter;
	private char take()
	{
		char c;
		if(repeated)
		{
			c=letter;
		}
		else
		{
			c=word.charAt(place);
			place++;
		}
		repeated=false;
		return c;
	}
	private void give(char C)
	{
		repeated=true;
		letter=C;
	}
	private String qs_word; // residue of quotedString
	private boolean quotedString(char Q)
	{
		int state;
		char c;
		boolean lacking,result;
		StringBuilder sb=new StringBuilder();
		state=0;
		lacking=result=true; // assume a %QS%
		c=Q; // set this for the beginning
		if(c!='\"') // everything else
		{
			give(c); // give back
		}
		else for(;;)
		{
			switch(state)
			{
			case 0:
				if(c=='\"')
				{
					state=1;
				}
				break;
			case 1:
				if(c=='\"')
				{
					state=2;
				}
				else // allowed everything but the separator
				{
					sb.append(c);
				}
				break;
			case 2:
				lacking=false;
				break;
			}
			if(!lacking)
			{
				give(c);
				break;
			}
			c=take(); // spurioius TAKE if not a QS
		}
		// about to return update the word
		qs_word=sb.toString();
		return result;
	}
	private HashMap<String,String> runAutomata()
	{
		HashMap<String,String> table=new HashMap<String,String>();
		char c;
		int state=0;
		boolean lacking=true;
		String first_value="";
		while(lacking)
		{
			c=take();
			switch(state)
			{
			case 0:
				if(c=='{')
					state=1;
				break;
			case 1:
				if(c=='}')
					state=5;
				else if(quotedString(c))
				{
					state=2;
					// save the key before its overwritten
					first_value=qs_word;
				}
				break;
			case 2:
				if(c==':')
					state=3;
				break;
			case 3:
				if(quotedString(c))
				{
					state=4;
					table.put(first_value,qs_word);
				}
				break;
			case 4:
				if(c==',')
					state=1;
				else if(c=='}')
				// the end condition
					lacking=false;
				break;
			default:
				throw new RuntimeException("UNKNOWN STATE");
			}
		}
		return table;
	}
	// is only level one since hash also doesn't have recursion
	private HashMap<String,String> table;
	ParseJSON(String S)
	{
		resetAutomata(S);
		table=runAutomata();
		/*
		// way to print the elements of the hash
		Set s=table.entrySet();
		Iterator i=s.iterator();
		while(i.hasNext())
		{
			Map.Entry me=(Map.Entry)i.next();
			System.out.println(me.getKey()+" "+me.getValue());
		}
		*/
	}
	String obtainValue(String K)
	{
		return(table.get(K));
	}
}
package com.example.bitwit;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.text.DecimalFormat;


public class PortfolioActivity extends AppCompatActivity {

    Database portfolio = new Database(this);

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.portfolio);

        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);

        int id = 0;
        Cursor cursor = portfolio.GetAllData();
        TableLayout table = (TableLayout)findViewById(R.id.TableLayout1);

        if(cursor.getCount() == 0)
        {
            TableRow row = new TableRow(this);
            row.setId(id++);
            TextView name = new TextView(this);
            String nameText = "null";
            name.setText(nameText);
            row.addView(name);

            table.addView(row);
            return;
        }

        int getName = cursor.getColumnIndex("COINNAME");
        int getCurrentCoinPrice = cursor.getColumnIndex("COINPRICE");
        int getCoinHeld = cursor.getColumnIndex("COINSHELD");
        int getMarketValue = cursor.getColumnIndex("MARKETVALUE");
        float totalInvestment = 0;
        float totalMarketValue = 0;

        while (cursor.moveToNext()) {

            TableRow row = new TableRow(this);
            row.setId(id++);

            TextView name = new TextView(this);
            name.setId(id++);
            String nameText = cursor.getString(getName);
            name.setText(nameText);
            row.addView(name);

            TextView coinPrice = new TextView(this);
            coinPrice.setId(id++);
            String coinPriceText = cursor.getString(getMarketValue);
            coinPrice.setText(coinPriceText);
            row.addView(coinPrice);

            TextView coinHeld = new TextView(this);
            coinHeld.setId(id++);
            String coinHeldText = String.valueOf(df.format(Float.valueOf(cursor.getString(getCoinHeld))));
            coinHeld.setText(coinHeldText);
            row.addView(coinHeld);

            TextView coinInvestment = new TextView(this);
            coinInvestment.setId(id++);
            String coinInvestmentText = cursor.getString(getCurrentCoinPrice);
            coinInvestment.setText(coinInvestmentText);
            row.addView(coinInvestment);

            TextView marketValue = new TextView(this);
            marketValue.setId(id++);
            String marketValueText = String.valueOf(df.format(Float.valueOf(cursor.getString(getMarketValue)) * Float.valueOf(cursor.getString(getCoinHeld))));
            marketValue.setText(marketValueText);
            row.addView(marketValue);

            TextView coinGain = new TextView(this);
            coinGain.setId(id++);
            String totalGainText = String.valueOf(df.format(Float.valueOf(cursor.getString(getMarketValue)) * Float.valueOf(cursor.getString(getCoinHeld)) - Float.valueOf(cursor.getString(getCurrentCoinPrice))));
            coinGain.setText(totalGainText);
            row.addView(coinGain);

            TextView totalGainPercent = new TextView(this);
            totalGainPercent.setId(id++);
            String totalGainPercentText = "%" + String.valueOf(df.format((Float.valueOf(cursor.getString(getMarketValue)) * Float.valueOf(cursor.getString(getCoinHeld)) / Float.valueOf(cursor.getString(getCurrentCoinPrice)) - 1) * 100));
            totalGainPercent.setText(totalGainPercentText);
            row.addView(totalGainPercent);

            table.addView(row);

            totalInvestment += Float.valueOf(cursor.getString(getCurrentCoinPrice));
            totalMarketValue += Float.valueOf(cursor.getString(getMarketValue)) * Float.valueOf(cursor.getString(getCoinHeld));
        }

        TextView totalInvestmentView = (TextView)findViewById(R.id.textView5);
        String totalInvestmentText = String.valueOf(totalMarketValue);
        totalInvestmentView.setText(totalInvestmentText);

        TextView totalMarketValueView = (TextView)findViewById(R.id.textView7);
        String totalIMarketValueText = String.valueOf(totalInvestment);
        totalMarketValueView.setText(totalIMarketValueText);

        TextView totalProfitView = (TextView)findViewById(R.id.textView10);
        String totalProfitText;

        if(totalMarketValue - totalInvestment >= 0)
            totalProfitText = "+" + String.valueOf(totalMarketValue - totalInvestment);
        else
            totalProfitText = String.valueOf(totalMarketValue - totalInvestment);

        totalProfitView.setText(totalProfitText);

    }




}
package com.example.bitwit;
import java.util.ArrayList;
class CurrencyList
{
	// in the future for a more optimized way
	//  by not using an ArrayList is to use
	//  a ring buffer with a circulating window
	//  so the newest value overwrites the oldest value
	private ArrayList<Double> prices;
	private int newest_index;
	private boolean first;
	final int MAX_NUM_ELEMENTS=10; // doesn't mean anything now, but will
	public CurrencyList()
	{
		prices=new ArrayList<Double>();
		prices.add(0.0); // so when read it doesn't give garbage
		newest_index=0;
		first=true;
	}
	public void append(double value)
	{
		// right now assumes "infinite" memory but in the
		//  future this will also remove the oldest once
		//  its memory has been maxed out
		if(first)
		{
			prices.remove(0);
			first=false;
		}
		else
		{
			newest_index++;
		}
		prices.add(value);
	}
	public double getNewest()
	{
		return prices.get(newest_index);
	}
	public double[] getRecentSlice(int maximum)
	{
		double[] costs;
		int i,j;
		costs=new double[Math.min(maximum,prices.size())];
		j=costs.length-1;
		for(i=0;i<costs.length;i++)
		{
			costs[j]=prices.get(newest_index-i);
			j--;
		}
		return costs;
	}
}

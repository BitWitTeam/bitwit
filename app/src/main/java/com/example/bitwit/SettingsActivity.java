package com.example.bitwit;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.List;

public class SettingsActivity extends AppCompatActivity {

    Database database = new Database(this);

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.settings);

        final Button button = findViewById(R.id.reset_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                database.clearDatabase(SettingsActivity.this);

                //hack way of getting coin list to display cleared fields
                //Intent goHome = new Intent(SettingsActivity.this, coin_list.class);
                //SettingsActivity.this.startActivity(goHome);
            }
        });
    }

}

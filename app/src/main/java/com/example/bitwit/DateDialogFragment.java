package com.example.bitwit;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;


public class DateDialogFragment extends DialogFragment {
	private DatePickerDialog.OnDateSetListener listener;
	public DateDialogFragment()
	{
	}
	public void usingListener(DatePickerDialog.OnDateSetListener dpdodsl)
	{
		listener=dpdodsl;
	}
	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Calendar c=Calendar.getInstance();
		int current_year=c.get(Calendar.YEAR);
		int current_month=c.get(Calendar.MONTH);
		int current_day=c.get(Calendar.DAY_OF_MONTH);
		DatePickerDialog dpd=new DatePickerDialog(
			getActivity(),
			listener,
			current_year,
			current_month,
			current_day
		);
		return dpd;
		//return super.onCreateDialog(savedInstanceState);
	}
}

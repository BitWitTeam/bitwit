package com.example.bitwit;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

// services connections to the CurrencyService
//  do not try and talk directly to the service without a connection
class CurrencyServiceConnection implements ServiceConnection
{
	private boolean activated;
	private CurrencyService utility;
	public CurrencyServiceConnection()
	{
		activated=false;
	}
	public boolean isActive()
	{
		return activated;
	}
	public CurrencyService acquire()
	{
		CurrencyService cs;
		cs=null;
		if(isActive())
		{
			cs=utility;
		}
		return cs;
	}
	@Override
	public void onServiceConnected(ComponentName name, IBinder service)
	{
		CurrencyService.Tie tie;
		tie=(CurrencyService.Tie)service;
		utility=tie.getService();
		activated=true;
	}
	@Override
	public void onServiceDisconnected(ComponentName name)
	{
		activated=false;
	}
}

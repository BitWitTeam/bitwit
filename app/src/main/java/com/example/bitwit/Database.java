package com.example.bitwit;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class Database extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "Coinwallet.db";

    private static final String TABLE_NAME = "coin_wallet";

    private static final String COLUMN_ID = "ID";

    private static final String COIN_NAME = "COINNAME";
    private static final String COIN_PRICE = "COINPRICE";
    private static final String COINS_HELD = "COINSHELD";
    private static final String MARKET_VALUE = "MARKETVALUE";

    public Database(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE;

        CREATE_CONTACTS_TABLE = "create table " + TABLE_NAME + " ("
                + COLUMN_ID +" INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COIN_NAME + " TEXT,"
                + COIN_PRICE + " INTEGER,"
                + MARKET_VALUE + " INTEGER,"
                + COINS_HELD + " INTEGER )";

        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    long Add(String coinName, String coinPrice, String coinsHeld, String marketValue) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COIN_NAME, coinName);
        values.put(COIN_PRICE, coinPrice);
        values.put(COINS_HELD, coinsHeld);
        values.put(MARKET_VALUE, marketValue);

        long id = db.insert(TABLE_NAME, null, values);
        db.close();

        return id;
    }

    public DataRetriever GetData(String coinName)
    {
        DataRetriever data = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query( TABLE_NAME, new String[]{COLUMN_ID, COIN_NAME, COIN_PRICE, COINS_HELD, MARKET_VALUE}, COIN_NAME + "=?",
                new String[]{coinName}, null, null, null, null);

        if(cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            data = new DataRetriever(
                    cursor.getString(cursor.getColumnIndex(COIN_NAME)),
                    cursor.getString(cursor.getColumnIndex(COIN_PRICE)),
                    cursor.getString(cursor.getColumnIndex(COINS_HELD)),
                    cursor.getString(cursor.getColumnIndex(MARKET_VALUE))
            );


        }
        else
        {
            //queried record does not exit....return empty object instead
            data = new DataRetriever("0","0", "0", "0");
        }

        cursor.close();
        return data;
    }

    public int updateCoin(DataRetriever data_in_database, float amount_bought_or_sold, float coins_held, float market_value) {
        SQLiteDatabase db = this.getWritableDatabase();

        Log.i("database","coin change: "+ String.valueOf(coins_held));

        if(amount_bought_or_sold < 0 && Math.abs(amount_bought_or_sold) > Float.valueOf(data_in_database.getCoinPrice()))
            return -1;

        ContentValues values = new ContentValues();
        values.put(COIN_NAME, data_in_database.getCoinName());
        values.put(COIN_PRICE, String.valueOf(Float.valueOf(data_in_database.getCoinPrice()) + amount_bought_or_sold));
        values.put(COINS_HELD, String.valueOf(Float.valueOf(data_in_database.getCoinsHeld()) + Float.valueOf(coins_held)) );
        values.put(MARKET_VALUE, market_value);

        return db.update(TABLE_NAME, values, COIN_NAME + " = ?",
                new String[]{data_in_database.getCoinName()});
    }

    public void clearDatabase(Context context) {
        context.deleteDatabase(DATABASE_NAME);
    }

    public Cursor GetAllData() {
        SQLiteDatabase db = getWritableDatabase();
        String query = "select * from " + TABLE_NAME;

        return db.rawQuery(query,null);
    }

}


package com.example.bitwit;

import java.util.HashMap;
class CoinNameResolver
// probably move this to its own file since it can be a helper class in the GUI
{
	private static Object[][] coinNames={
	//private static String[][] coinNames={
		// the columns of this array are as follows and shouldn't be changed
		//  because of earlier code
		// {
		// <abbreviated coin name>,<full coin name>,
		// <valid website flag>,<has historical data>
		// }
		// add names here, should there be another entry for API names?
		//  or will API names always be the same as names used in the program?
		// COINGECKO
		{"DOGE","DogeCoin",CoinNameResolver.COINGECKO,true,1391275478733L},
		{"GRLC","Garlicoin",CoinNameResolver.COINGECKO,false,0L},
		{"RDD","ReddCoin",CoinNameResolver.COINGECKO,false,0L},
		{"ANI","Animecoin",CoinNameResolver.COINGECKO,false,0L},
		// BITFINEX
		{"BTC","Bitcoin",CoinNameResolver.BITFINEX|CoinNameResolver.COINGECKO,true,1375374147814L},
		{"ETH","Ethereum",CoinNameResolver.BITFINEX|CoinNameResolver.COINGECKO,true,1443716566838L},
		{"ZEC","Zcash",CoinNameResolver.BITFINEX|CoinNameResolver.COINGECKO,true,1480612982762L},
		{"BCH","BtcCash",CoinNameResolver.BITFINEX|CoinNameResolver.COINGECKO,true,1509553407728L},
		{"ETC","EthClassic",CoinNameResolver.BITFINEX|CoinNameResolver.COINGECKO,true,1472747024245L},
		{"XMR","Monero",CoinNameResolver.BITFINEX|CoinNameResolver.COINGECKO,true,1412180648642L},
		{"XRP","Ripple",CoinNameResolver.BITFINEX|CoinNameResolver.COINGECKO,true,1388597063839L},
		/*
		// removed to conserve bandwidth on bitfinex
		{"IOT","InetOfThngs"},
		{"OMG","OmiseGO"},
		{"BFT","BnkTheFut"},
		*/
	};
	private static final int SHORTENED=0;
	public static final int REAL=1;
	private static String[] getNameArray(int index)
	// FUNCTION IS ONLY VALID FOR index={0,1}
	{
		String[] names=new String[coinNames.length];
		int i;
		for(i=0;i<names.length;i++)
		{
			names[i]=(String)coinNames[i][index];
		}
		// get all the three letter names to build
		return names;
	}
	public static String[] getShortenedNameArray()
	{
		return getNameArray(SHORTENED);
	}
	public static String[] getRealNameArray()
	{
		return getNameArray(REAL);
	}
	public static CoinNameResolver getInstance()
	{
		return new CoinNameResolver();
	}
	// could make this Singleton as it doesn't change once built
	HashMap<String,String> nickname_lookup_table;
	HashMap<String,String> fullname_lookup_table;
	HashMap<String,Integer> url_flags_table;
	HashMap<String,Boolean> historical_data_table;
	HashMap<String,Long> creation_table;
	// will be used by the API query to build
	//  a request
	public CoinNameResolver()
	{
		// constants to show how table is made
		final int SHORT=0;
		final int FULL=1;
		// make the table
		//  using the short name into the long name
		nickname_lookup_table=new HashMap<String,String>();
		for(int coin_index=0;coin_index<coinNames.length;coin_index++)
		{
			nickname_lookup_table.put(
				(String)coinNames[coin_index][FULL],
				(String)coinNames[coin_index][SHORT]
			);
		}
		// populate the inverse table
		fullname_lookup_table=new HashMap<String,String>();
		for(int coin_index=0;coin_index<coinNames.length;coin_index++)
		{
			fullname_lookup_table.put(
					(String)coinNames[coin_index][SHORT],
					(String)coinNames[coin_index][FULL]
			);
		}
		final int URL_FLAGS=2;
		url_flags_table=new HashMap<>();
		for(int coin_index=0;coin_index<coinNames.length;coin_index++)
		{
			url_flags_table.put(
					(String)coinNames[coin_index][SHORT],
					(Integer)coinNames[coin_index][URL_FLAGS]
			);
		}
		final int WHETHER_HISTORICAL=3;
		historical_data_table=new HashMap<>();
		for(int coin_index=0;coin_index<coinNames.length;coin_index++)
		{
			historical_data_table.put(
					(String)coinNames[coin_index][SHORT],
					(Boolean)coinNames[coin_index][WHETHER_HISTORICAL]
			);
		}
		final int GENESIS_BLOCK=4;
		creation_table=new HashMap<>();
		for(int coin_index=0;coin_index<coinNames.length;coin_index++)
		{
			creation_table.put(
					(String)coinNames[coin_index][SHORT],
					(Long)coinNames[coin_index][GENESIS_BLOCK]
			);
		}
	}
	public boolean isValidName(String coin_name)
	{
		// check in coin_names
		//  check with the full names
		boolean result;
		result=(nickname_lookup_table.get(coin_name.toUpperCase())==null)
			?
			false:true
		;
		return result;
	}
	public String getShortenedFromFull(String coin_name)
	{
		String abbreviation;
		// throw an exception if not there?
		abbreviation=nickname_lookup_table.get(coin_name);
		if(abbreviation==null)
			abbreviation="?";
		return abbreviation;
	}
	public String getFullFromShortened(String ticker_name)
	{
		String real_name;
		real_name=fullname_lookup_table.get(ticker_name);
		return real_name;
	}
	public boolean isMemeCurrency(String ticker_name)
	// must use coin gecko if this is the case
	{
		Integer flags;
		flags=url_flags_table.get(ticker_name.toUpperCase());
		return ((flags&CoinNameResolver.COINGECKO)>0);
	}
	public boolean isNothingButMemeCurrency(String ticker_name)
	// must use coin gecko if this is the case
	{
		Integer flags;
		flags=url_flags_table.get(ticker_name.toUpperCase());
		return (flags==CoinNameResolver.COINGECKO);
	}
	public String CoinGeckoURLName(String ticker_name)
	{
		// for those special cases
		if(ticker_name.compareTo("BCH")==0)
		{
			return "bitcoin-cash";
		}
		else if(ticker_name.compareTo("ETC")==0)
		{
			return "ethereum-classic";
		}
		else
		{
			return getFullFromShortened(ticker_name);
		}
	}
	public boolean hasHistorical(String ticker_name)
	{
		return historical_data_table.get(ticker_name);
	}
	public long getCreation(String ticker_name)
	{
		return creation_table.get(ticker_name);
	}
	private static CoinNameResolver cnr=null;
	public static CoinNameResolver acquireSingleton()
	{
		if(CoinNameResolver.cnr==null)
		{
			cnr=new CoinNameResolver();
		}
		return cnr;
	}
	public static final int BITFINEX=1<<0;
	public static final int COINGECKO=1<<1;
	public static String buildApiUrl(String symbol_name,int mode)
	{
		// should incoming name be checked for concistency?
		StringBuilder url=new StringBuilder();
		url.append("https://"); // haven't found one yet only on HTTP
		switch(mode)
		{
		case BITFINEX:
			// wants short abbreviated names
			url.append("api.bitfinex.com/v1/pubticker/");
			url.append(symbol_name.toLowerCase()); // should check if valid?
			url.append("usd");
			break;
		case COINGECKO:
			// coin gecko wants lower case long names
			CoinNameResolver cnr;
			cnr=CoinNameResolver.acquireSingleton();
			url.append("www.coingecko.com/en/widget_component/ticker/");
			url.append(cnr.CoinGeckoURLName(symbol_name).toLowerCase());
			//url.append("dogecoin");
			url.append("/usd");
			break;
		//default: throw exception?
		}
		//https://api.bitfinex.com/v1/pubticker/btcusd
		//https://www.coingecko.com/en/widget_component/ticker/dogecoin/usd
		return url.toString();
	}
	public static String buildDefaultApiUrl(String name)
	{
		// using bitfinex as the default
		return buildApiUrl(name,BITFINEX);
	}
}

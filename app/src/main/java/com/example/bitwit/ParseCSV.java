package com.example.bitwit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class ParseCSV
{
	// understands the first row is the names
	//  subsequent rows 
	public ParseCSV(String script)
	{
		repeat_letter=false;
		letter_to_repeat='\0';
		sentence=script;
		sentence_place=0;
		database=new HashMap<>();
		build();
	//	System.out.println(database.values());
	}
	private HashMap<String,String[]> database;
	private void build()
	{
		int index;
		List<String> header=new LinkedList<>();
		do
		{
			EN();
		//	System.out.println(en_word);
			header.add(en_word);
		}
		while(en_mode!=ROW_END);
		List<List<String>> data=new ArrayList<>();
		for(index=0;index<header.size();index++)
		{
			ArrayList<String> piece;
			piece=new ArrayList<String>();
			data.add(piece);
		}
		index=0;
		do
		{
			EN();
			data.get(index).add(en_word);
		//	System.out.println(en_word);
			index=(++index)%header.size();
		}
		while(sentence_place<sentence.length());
		// have all the data loaded, build the hash map
		for(index=0;index<header.size();index++)
		{
			String[] tray=new String[data.get(index).size()];
			tray=data.get(index).toArray(tray);
			database.put(
				header.get(index),
				tray
			);
		}
	}
	private static final int COLUMN_END=0; // column
	private static final int ROW_END=1; // newline
	private String en_word;
	private int en_mode;
	private void EN()
	{
		StringBuilder sb=new StringBuilder();
		char c;
		for(;;)
		{
			c=take();
			if(c==',')
			{
				en_mode=COLUMN_END;
				break;
			}
			else if(c=='\n'||c=='\r')
			{
				do c=take(); while(c=='\n'||c=='\r');
				give(c);
				en_mode=ROW_END;
				break;
			}
			else
			{
				sb.append(c);
			}
		}
		en_word=sb.toString();
	}
	private boolean repeat_letter;
	private char letter_to_repeat;
	private String sentence;
	private int sentence_place;
	private char take()
	{
		char c;
		if(repeat_letter)
		{
			repeat_letter=false;
			return letter_to_repeat;
		}
		if(sentence_place<sentence.length())
		{
			c=sentence.charAt(sentence_place);
			sentence_place++;
		}
		else
		{
			c='\0';
		}
		return c;
	}
	private void give(char C)
	{
		repeat_letter=true;
		letter_to_repeat=C;
	}
	private void criticalError()
	{
		throw new RuntimeException("PROBLEM!");
	}
	public String[] collect(String key)
	{
		return database.get(key);
	}
}
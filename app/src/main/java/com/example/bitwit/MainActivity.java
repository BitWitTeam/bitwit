package com.example.bitwit;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.AnimationDrawable;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ServiceConfigurationError;
import java.util.Timer;
import java.util.TimerTask;

//Test push to develop
public class MainActivity extends AppCompatActivity {
	Button button; // when this needs to be manual
	private ProgressBar loading_bar;
	private class LoaderDrawer extends TimerTask
	{
		private int count;
		private Timer ticker;
		private final int TICKS_UNTIL_CHANGE=10;
		private boolean all_set_up;
		private String[] abbreviations;
		private CurrencyService server;
		private AppCompatActivity associate;
		public LoaderDrawer(Timer ticker,AppCompatActivity activity)
		{
			count=0;
			this.ticker=ticker;
			abbreviations=CoinNameResolver.getShortenedNameArray();
			server=null;
		//	double d=cs.getLatest("BTC");
			associate=activity;
		}
		@Override
		public void run()
		{
			boolean ready;
			server=hookup.acquire();
			ready=true; // assume ready
			if(server!=null)for(String coin:abbreviations)
			{
				if(server.getLatest(coin)==0.0)
				{
					ready=false;
					break; // wait some more until all coins come in
				}
			}
			// title
			if(associate!=null)
			{
			//	associate.getActionBar().setTitle("WHATEVER"+count);
			}
			if(
					(ready&&(count>2))
					||
					count>TICKS_UNTIL_CHANGE
			)
			{
				loading_bar.setProgress(100); // done with it now
				ticker.cancel(); // since now done with the events, don't want anymore
				Intent i=new Intent(MainActivity.this,coin_list.class);
				startActivity(i);
				finish(); // prevents back going to this, no more loading screen
			}
			else
			{
				//TextView tv=(TextView)findViewById(R.id.textView3);
				//tv.setText("");//Integer.toString(count));
				count++;
				loading_bar.setProgress(10*count);
				loading_bar.postInvalidate();
			}
		}
	}
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		this.getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,R.layout.custom_titlebar);
		TextView tv=(TextView)findViewById(R.id.title_right_text);
	//	tv.setText(getTitle().toString());
		// create the only currency connection
		hookup=new CurrencyServiceConnection();
		Timer ticker=new Timer();
		TimerTask loader=new LoaderDrawer(ticker,this);
		//t.schedule(tt,1000);
		ticker.schedule(loader,0,1000);
		/*
		int count;
		count=0;
		while(!is_service_bound)
		{
		}
		Toast.makeText(this,"Hooked",Toast.LENGTH_SHORT).show();
		*/
		/*
		// no button anymore
		button=(Button)this.findViewById(R.id.loading_button);
		button.setOnClickListener(new View.OnClickListener()
								  {
			@Override
			public void onClick(View arg0)
			{
			//	Intent mine=new Intent(
			//	MainActivity.this,
				//BuyPage.class // layout works
				//coin_list.class // works fully now
			//	coin_list.class
				//calculator.class
			//	);
			//	startActivity(mine);
				CurrencyService cs=hookup.acquire();
				double d=cs.getLatest("BTC");
				Toast.makeText(getApplicationContext(),"Did"+Double.toString(d),Toast.LENGTH_SHORT).show();
			}
								  }
		);
		*/
		// loading bar
		loading_bar=(ProgressBar)findViewById(R.id.progressBar);
		// animation on loading screen
		ImageView iv=(ImageView)findViewById(R.id.animatedLogo);
		//iv.setImageResource(R.drawable.coin0);
		/*
		// these two alone made it red
		iv.setImageDrawable(null);
		iv.setBackgroundColor(0xFF0000);
		*/
		//iv.invalidate();
		iv.setImageResource(R.drawable.load_anim);
		//iv.setBackgroundResource(R.drawable.load_anim);
		AnimationDrawable ad=(AnimationDrawable)iv.getDrawable();

		ImageView secondAnimView = (ImageView) findViewById(R.id.animatedLogo2);
		secondAnimView.setImageResource(R.drawable.load_anim);
		AnimationDrawable ad2 = (AnimationDrawable) secondAnimView.getDrawable();

		ImageView thirdAnimView = (ImageView) findViewById(R.id.animatedLogo3);
		thirdAnimView.setImageResource(R.drawable.load_anim);
		AnimationDrawable ad3 = (AnimationDrawable) thirdAnimView.getDrawable();

		ImageView fourthAnimView = (ImageView) findViewById(R.id.animatedLogo4);
		fourthAnimView.setImageResource(R.drawable.load_anim);
		AnimationDrawable ad4 = (AnimationDrawable) fourthAnimView.getDrawable();


		ad.start();
		ad2.start();
		ad3.start();
		ad4.start();
	}
	protected void onStart()
	{
		super.onStart();
		Intent goal=new Intent(this,CurrencyService.class);
		/*
		startService(
				goal
		);
		*/
		boolean real;
		real=bindService(
				goal,
				hookup,
				Context.BIND_AUTO_CREATE
		);
	}
	protected void onDestroy()
	{
		super.onDestroy();
		unbindService(hookup);
	}
	private CurrencyServiceConnection hookup;
}

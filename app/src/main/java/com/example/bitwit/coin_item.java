package com.example.bitwit;


/**
 * Created by juan on 3/7/2018.
 */

public class coin_item {

    private String coinName;
    private String coinNameAbv;
    private double coinPrice;
    private double coinsHeld;
    private double moneyInvested;

    private boolean coinFavortieboolean;

    public coin_item(String coinName,String coinNameAbv, double coinPrice, double coinsHeld, boolean coinFavortieboolean) {
        this.coinName = coinName;
        this.coinNameAbv = coinNameAbv;
        this.coinPrice = coinPrice;
        this.coinsHeld = coinsHeld;
        this.coinFavortieboolean = coinFavortieboolean;
        this.moneyInvested = 0.0;
    }

    public String getCoinName() {
        return coinName;
    }

    public String getCoinNameAbv()
    {
        return coinNameAbv;
    }

    public double getCoinPrice() {

        return coinPrice;
    }

    public double getCoinsHeld() {
        return coinsHeld;
    }

    public double getMoneyInvested() {
        return moneyInvested;
    }

    public boolean isCoinFavortieboolean() {
        return coinFavortieboolean;
    }

    public void setCoinFavortieboolean(boolean coinFavortieboolean) {
        this.coinFavortieboolean = coinFavortieboolean;
    }

    public void setCoinPrice(double coinPrice) {
        this.coinPrice = coinPrice;
    }

    public void setMoneyInvested(double moneyInvested, double coinsHeld) {
        this.moneyInvested = moneyInvested;
        this.coinsHeld = coinsHeld;
    }
}
package com.example.bitwit;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.net.ssl.HttpsURLConnection; // since JAVA4
class WebsiteVisitor
{
	private final int BLOCK_SIZE=1024; // block reading size
	private String address_name;
	private byte[] data_block;
	private URL site;
	private HttpsURLConnection leach;
	public WebsiteVisitor(String url_name)
	{
		address_name=url_name;
		data_block=new byte[BLOCK_SIZE];
		try
		{
			site=new URL(url_name);
			leach=(HttpsURLConnection)site.openConnection();
		}
		catch(MalformedURLException murle)
		{
			throw new RuntimeException(murle.toString());
		}
		catch(IOException ioe)
		{
			throw new RuntimeException(ioe.toString());
		}
	}
	private String readEverythingAndClose(InputStream remote)
	{
		StringBuilder sb=new StringBuilder();
		try
		{
			int count=0;
			while((count=remote.read(data_block,0,BLOCK_SIZE))>0)
			{
				if(count==BLOCK_SIZE)
				// can use the whole block
				{
					sb.append(new String(data_block));
				}
				else // count<BLOCK_SIZE and count>0
				// read the rest
				{
					byte[] carrier; // so the memory isn't released from data_block
					carrier=Arrays.copyOfRange(data_block,0,count);
					sb.append(new String(carrier));
				}
				//	System.out.println("####### MY COUNT "+count);
			}
			remote.close();
		}
		catch(IOException ioe)
		{
			throw new RuntimeException(ioe.toString());
		}
		return sb.toString();

	}
	public final static String TRAIT_AGENT="User-Agent";
	public final static String AGENT_MOZILLA4=
			"Mozilla/4.0 (compatible; MSIE 4.5; Mac_PowerPC)"
			//	"Mozilla/4.0 (Windows; MSIE 7.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727)"
			;
	public final static String AGENT_MOZILLA5=
			"Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko"
			;
	public void setTrait(String key,String value)
	{
		// DO NOT USE THIS WITH BITFINEX
		//  it will think you're apart of DDOS
		leach.setRequestProperty(key,value);
	}
	public String callSiteGiveOutput()
	{
		String received="";
		try
		{
			// interestingly enough the "available" for the InputStream
			//  can be a lie, sometimes used and sometimes not used
			received=readEverythingAndClose(
					leach.getInputStream() // does the actual connection
			);
			//System.out.println("CONNECTION COMPLETED");
			leach.disconnect();
		}
		catch(IOException ioe)
		{
			// happens when HTTP response code 429 which means to many requests
			//  can also happen because of not connected to the internet
			throw new RuntimeException(ioe.toString());
		}
		return received;
	}
	public HashMap<String,String> callSiteGiveCookieAndCrumb()
	{
		HashMap<String,String> details=new HashMap<>();
		try
		{
			/*
			String address_name="https://finance.yahoo.com/quote/BTC-USD/history/";
			URL site=new URL(address_name);
			HttpsURLConnection leach=(HttpsURLConnection)site.openConnection();
			*/
			// get at the cookie jar
			String data=readEverythingAndClose(leach.getInputStream());
			/*
			Set<String> l=leach.getHeaderFields().keySet();//.get("Set-Cookie");
			for(String c:l)
			{
				System.out.println(c);
			}
			System.out.println("VALUE OF: "+leach.getHeaderFields().get("set-cookie"));
			*/
			// assuming only one, since input was one
			String cookie=leach.getHeaderFields().get("set-cookie").get(0);
			HttpCookie hc=HttpCookie.parse(cookie).get(0);
			//	System.out.println(hc.getName());
			//	System.out.println(hc.getValue());
			final String known_crumb="\"CrumbStore\"";
			int beginning,ending;
			beginning=data.indexOf(known_crumb);
			beginning+=known_crumb.length();
			beginning++; // move past the ':'
			// the first '}' is the end of the JSON
			ending=data.indexOf('}',beginning);
			ending++; // include itself
			// index returned is at the start of this
			String crumb_value=data.substring(beginning,ending);
			//	System.out.println(crumb_value);
			crumb_value=crumb_value.substring(
					crumb_value.indexOf(':')+2,
					crumb_value.indexOf('}')-1
			);
			//	System.out.println(crumb_value);
			String un=TextUtility.Unescape(crumb_value);
			if(un.compareTo(crumb_value)!=0)
			{
				crumb_value=un;
				//		System.out.println(un);
				//		System.out.println("\t####### IT WORKED THROUGH THE IMPASS!###");
				// CONFIRMED WORKING FOR AN ESCAPE AT INDEX 0
			}
			// the crumb may contain an octal value, should it be sent
			//  as it or converted?
			// It would seem it will not work with the octal number
			//  and so must be formated, is there a method for this or must one be developed?
			// works with BTC,DOGE,ETH,ETC,XMR,XRP,ADA
			/*
			address_name="https://query1.finance.yahoo.com/v7/finance/download/BTC-USD";
			StringBuilder sb=new StringBuilder();
			sb.append(address_name);
			sb.append('?');
			sb.append("period1=1523131306");
			sb.append('&');
			sb.append("period2=1525723306");
			sb.append('&');
			sb.append("interval=1d");
			sb.append('&');
			sb.append("events=history");
			sb.append('&');
			sb.append("crumb=");
			sb.append(crumb_value);
			*/
			details.put("Crumb",crumb_value); // only the value
			details.put("Cookie",hc.getName()+"="+hc.getValue()); // the format of a cookie
			/*
			// now handled by caller
			site=new URL(sb.toString());
			leach=(HttpsURLConnection)site.openConnection();
			leach.setRequestProperty("Cookie",hc.getName()+"="+hc.getValue());
			data=readEverythingAndClose(leach.getInputStream());
			System.out.println("GOT BACK: "+data.length());
		//	System.out.println(data);
			ParseCSV pcsv=new ParseCSV(data);
			String[] values=pcsv.collect("Close");
			for(String v:values)
			{
			//	System.out.println(v);
			}
			*/
		}
		catch(Exception E)
		{
			E.printStackTrace();
		}
		return details;
	}
}
package com.example.bitwit;
public class TextUtility
{
	public static boolean isLetter(char C)
	{
		if(
			(C>='A'&&C<='Z')
			||
			(C>='a'&&C<='z')
		)return true;
		else return false;
	}
	public static boolean isNumber(char C)
	{
		if(C>='0'&&C<='9')return true;
		else return false;
	}
	public static boolean isWhitespace(char C)
	{
		if(
			C==' '||C=='\t'||C=='\n'
		)return true;
		else return false;
	}
	private static boolean isHexidecimalDigit(char C)
	{
		if(
			TextUtility.isNumber(C)
			||
			(C>='A'&&C<='F')||(C>='a'&&C<='f')
		)return true;
		else return false;
	}
	public static String Unescape(String escaped)
	// converts the escaped hex characters to
	//  their raw equivalents using this is as a reference
	//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Grammar_and_types
	// as well as using design from
	//https://github.com/jashkenas/coffeescript/issues/3795
	{
	//	"\u002F" // looking for like this
		char c;
		int i;
		char[] slider=new char[6];
		char[] numbers=new char[4]; // copy of the hex values for Integer.parseInt
		StringBuilder sb=new StringBuilder();
		int matched;
		int state;
		matched=0;
		state=0;
		for(i=0;i<escaped.length();i++)
		// build a sliding window and validate, if cannot be then write
		{
			c=escaped.charAt(i);
			//System.out.println("CHAR: "+c);
			if(c=='\\')
			{
				slider[matched]=c;
				matched=1;
			}
			else if(c=='u'&&matched==1)
			{
				slider[matched]=c;
				matched=2;
			}
			else if(TextUtility.isHexidecimalDigit(c)&&matched>=2)
			{
				slider[matched]=c;
				// since matched must equal 2 to get in here
				numbers[matched-2]=c;
				matched++;
			}
			else
			{
				sb.append(c); // append the now character
				for(int j=0;j<matched;j++)
				// invalidate the window
				{
					sb.append(slider[j]);
				}
				matched=0; // orphan it
			}
			if(matched==6)
			{
				short ordinal;
				// do the operation
				ordinal=(short)Integer.parseInt(new String(numbers),16);
				sb.append((char)ordinal); // should always work?
				matched=0;
			}
		}
		if(matched>0)
		{
			for(int j=0;j<matched;j++)
				sb.append(slider[j]); // read it the rest wasn't a valid unicode escape
		}
		return sb.toString();
	}
}

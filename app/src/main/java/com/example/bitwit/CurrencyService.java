package com.example.bitwit;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class CurrencyService extends Service
{
	private IBinder singleton_binder; // should this be final?
	private CurrencyManager cm;
	private Random whatever;
	private int days_past;
	public CurrencyService()
	{
		singleton_binder=new Tie();
		whatever=new Random();
		historicals=new HashMap<>();
		signalers=new HashMap<>();
		data_acessor=new Semaphore(1); // used for accessing thread data
		days_past=0;
	}
	public void onCreate()
	// runs before onStartCommand
	{
		//String[] stuff={"BTC","ETH"};
		String[] stuff=CoinNameResolver.getShortenedNameArray();
		cm=new CurrencyManager(stuff);
	}
	public void onDestroy()
	{
		cm.destroy();
	}
	public int onStartCommand(Intent goal,int flags,int id)
	{
		return START_STICKY;
	}
	public class Tie extends Binder
	{
		CurrencyService getService()
		{
			return CurrencyService.this;
		}
	}
	public IBinder onBind(Intent intent)
	{
		return singleton_binder;
	}
	public int getRandomNumber()
	{
		return whatever.nextInt();
	}
	public double getLatest(String currency_name)
	{
		return cm.getLatest(currency_name);
	}
	public double[] getMostRecentSlice(String currency_name,int maximum)
	{
		return cm.getMostRecentSlice(currency_name,maximum);
	}
	// need synchronization so using HashMap
	Semaphore data_acessor;
	HashMap<String,double[]> historicals;
	HashMap<String,Semaphore> signalers;
	// as of right now there is only one format for HistoricalData
	//  and that is in the last 30 days
	public void requestHistoricalData(String currency_name)
	{
		CoinNameResolver cnr;
		cnr=CoinNameResolver.acquireSingleton();
		if(!cnr.hasHistorical(currency_name))
			return; // no reason to try if can't
		// who is going to do this asynchronously?
		// really don't need to include the CurrencyManager in it
		// If I can create a thread here that'll do the action and set
		//  a value for the poll, I only have to store it
		data_acessor.acquireUninterruptibly();
		if(signalers.get(currency_name)==null)
		// currency doesn't exist so make a thread and semaphore
		{
			signalers.put(
					currency_name,
					new Semaphore(1)
			);
			// create a new thread for this new currency
			HistoricalThread ht;
			ht=new HistoricalThread(currency_name);
			ht.start(); // now the other thread is running
		}
		data_acessor.release();
	}
	public double[] pollHistoricalData(String currency_name)
	{
		double[] values;
		values=null;
		// get the semaphore for this to check if its signaled
		//  if so then data exists for it, if not then it is not ready
		// check a table of semaphores to see if they are set, if so
		//  then has historical data
		data_acessor.acquireUninterruptibly();
		Semaphore accessor=signalers.get(currency_name);
		if(accessor!=null)
		{
			if(accessor.tryAcquire())
			// if I acquired, I need to get rid of the lock
			{
				accessor.release();
			}
			else
			// was already signaled so get the value
			{
				values=historicals.get(currency_name);
			}
		}
		data_acessor.release();
		return values;
	}
	// this is where the historical data should be stored, but it shouldn't be gotten from here
	private class HistoricalThread extends Thread
	{
		private static final String historical_site="https://finance.yahoo.com/quote/";
		private static final String data_site="https://query1.finance.yahoo.com/v7/finance/download/";
		private String local_name;
		public HistoricalThread(String ticker_name)
		{
			local_name=ticker_name;
		}
		public void run()
		{
			double[] prices;
			WebsiteVisitor cooker=new WebsiteVisitor(historical_site);
			HashMap<String,String> cache=cooker.callSiteGiveCookieAndCrumb();
			StringBuilder url=new StringBuilder();
			url.append(data_site);
			url.append(local_name);
			url.append("-USD");
			url.append('?');
			//sb.append("period1=1523131306");
			url.append("period1=");
			url.append(TimeUtility.rememberAsTextInSeconds(30+days_past));
			url.append('&');
			//sb.append("period2=1525723306");
			url.append("period2=");
			url.append(TimeUtility.rememberAsTextInSeconds(0+days_past));
			url.append('&');
			url.append("interval=1d");
			url.append('&');
			url.append("events=history");
			url.append('&');
			url.append("crumb=");
			url.append(cache.get("Crumb"));
			WebsiteVisitor crawler=new WebsiteVisitor(url.toString());
			crawler.setTrait("Cookie",cache.get("Cookie"));
			String data;
			data="";
			try
			{
				data=crawler.callSiteGiveOutput();
			}
			catch(Exception E)
			{
				// need to remove Semaphore to force an invalidation
				signalers.remove(local_name); // must be there to, run this thread
				// no matter what the site says, just don't let the app crash
				return;
			}
		//	System.out.println(data);
			ParseCSV pcsv=new ParseCSV(data);
			String[] text_prices=pcsv.collect("Close");
			prices=new double[text_prices.length];
			for(int i=0;i<text_prices.length;i++)
			{
				prices[i]=Double.parseDouble(text_prices[i]);
			//	System.out.print(prices[i]+", ");
			}
		//	System.out.println();
			data_acessor.acquireUninterruptibly();
			historicals.put(local_name,prices); // insert this array
			Semaphore accessor=signalers.get(local_name); // has to exist
			accessor.acquireUninterruptibly(); // essentially signaled as an event now
			data_acessor.release();
		}
	}
	public void invalidateHistoricalDataTo(String currency_name,int days_past)
	{
		data_acessor.acquireUninterruptibly();
		this.days_past=days_past;
		Semaphore accessor=signalers.get(currency_name);
		if(accessor!=null)
		{
			signalers.remove(currency_name); // get rid of it, for poll to put it back
			accessor.drainPermits();
		}
		data_acessor.release();
	}
}

package com.example.bitwit;

import java.util.Random;
class DataObtainer
{
	private SimilarRandoms sr;
	private boolean use_rng;
	public DataObtainer()
	{
		sr=new SimilarRandoms(0.01);
		use_rng=false;
	}
	public void setUseFakeData(boolean value)
	// use RNG or query website
	{
		use_rng=value;
	}
	public double deliver(String coin_name)
	{
		if(use_rng)
		{
			double d=sr.nextSimilar();
		//	int x=(int)(d*100.0);
		//	d=x/100.0;
		//	if(x%2==0)throw new RuntimeException("Happened");
			return d;
		}
		else
		{
			CoinNameResolver cnr=CoinNameResolver.acquireSingleton();
			if(cnr.isNothingButMemeCurrency(coin_name))
			{
				String url;
				url=CoinNameResolver.buildApiUrl(coin_name,CoinNameResolver.COINGECKO);
				WebsiteVisitor wv=new WebsiteVisitor(url);
				wv.setTrait(
						WebsiteVisitor.TRAIT_AGENT,
						WebsiteVisitor.AGENT_MOZILLA4
				);
				ParseHTML phtml=new ParseHTML(wv.callSiteGiveOutput());
				String price=phtml.acquire("/html/body/div/div/div/span/span");
				price=price.substring(1); // remove the '$'
				double value=Double.valueOf(price);
				return value;
			}
			else
			{
				// coin_name would come in, use the instance of the CoinNameResolver
				//  to get its path and then return that value, if wanting to get values
				//  from different websites it'd make the most sense to that here automatically
				// here is where the "Crawler" will be called and also the JSON
				//  parser will be used to extract a value
				double price=0.0;
				try
				{
					WebsiteVisitor crawler=new WebsiteVisitor(CoinNameResolver.buildDefaultApiUrl(coin_name));
					String yield=crawler.callSiteGiveOutput(); // this is in JSON
					ParseJSON parser=new ParseJSON(yield);
					String price_text=parser.obtainValue("ask");
					price=Double.parseDouble(price_text);
				}
				catch(Exception re)
				{
					// try with coin gecko
					if(cnr.isMemeCurrency(coin_name))
						price=obtainWithCoinGecko(coin_name);
				}
				return price;
			}
		}
	}
	private double obtainWithCoinGecko(String coin_name)
	{
		String url;
		url=CoinNameResolver.buildApiUrl(coin_name,CoinNameResolver.COINGECKO);
		WebsiteVisitor wv=new WebsiteVisitor(url);
		wv.setTrait(
				WebsiteVisitor.TRAIT_AGENT,
				WebsiteVisitor.AGENT_MOZILLA4
		);
		ParseHTML phtml=new ParseHTML(wv.callSiteGiveOutput());
		String price=phtml.acquire("/html/body/div/div/div/span/span");
		price=price.substring(1); // remove the '$'
		price=price.replaceAll(",","");
		double value=Double.valueOf(price);
		return value;
	}
	private class SimilarRandoms
	{
		Random source;
		private double seed;
		private double loop;
		public SimilarRandoms(double loop)
		{
			this.loop=loop;
			source=new Random();
			seed=Math.abs(source.nextDouble())*this.loop;
		}
		public double nextSimilar()
		{
			double unbounded;
			// 10 percent of the last value
			unbounded=Math.abs(source.nextDouble())*this.loop;
			seed=(seed*0.90)+(unbounded*0.10);
			return seed;
		}
	}
}
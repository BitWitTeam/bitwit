package com.example.bitwit;

import java.util.*;
import java.text.*;
class TimeUtility
{
	final static long millis_in_day=24*60*60*1000;
	public static String build(long millis)
	{
		Calendar c=Calendar.getInstance();
		c.setTimeInMillis(millis);
		DateFormat df=new SimpleDateFormat(
		//	"yyyy/MM/dd HH:mm:ss"
			"MM/dd/yyyy"
		);
		return df.format(c.getTime());
	}
	public static long remember(int days_ago)
	// find the unix time of a certain number of days ago
	{
		Calendar c=Calendar.getInstance();
		long count=System.currentTimeMillis();
		// from the unix time of now subtract by the number of days
		count-=(days_ago*millis_in_day);
		c.setTimeInMillis(count);
		return c.getTimeInMillis();
	}
	public static String rememberAsTextInSeconds(int days_ago)
	{
		long count;
		count=TimeUtility.remember(days_ago);
		count/=1000; // returned is in milliseconds require seconds
		return String.format("%d",count);
	}
	public static int daysBetween(long present,long past)
	{
		long difference;
		difference=present-past;
		difference/=millis_in_day;
		return(int)((difference>0)?difference:0);
	}
	/*
	public static void main(String[] ArgS)
	{
		long b=remember(0);
		long e=remember(30);
		System.out.println(b);
		System.out.println(build(b));
		System.out.println(e);
		System.out.println(build(e));
	}
	*/
}
package com.example.bitwit;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.JsonReader;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;


import static android.os.SystemClock.sleep;

public class coin_list extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private List<coin_item> coinItem;
    List<coin_item> sortedCoinList = new ArrayList<>();
    SwipeRefreshLayout mCoinListRefresh;
    Database database = new Database(this);

    // the connection overhead
	private CurrencyServiceConnection hookup;
	private RecyclerThread updater;
	private Semaphore kill_event;
	private boolean activated;

	private String[] coinNamesAbv;
	private String[] fullCoinName;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_coin_list);
		// create the service connection
		hookup=new CurrencyServiceConnection();

        mCoinListRefresh = (SwipeRefreshLayout) findViewById(R.id.coinListRefresh);
        mCoinListRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                saveFavoriteCoinStringListToFile(sortedCoinList);
                finish();
                startActivity(getIntent());
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.cointListing);

        //set if content does not change layout size....improves performace
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        coinItem = new ArrayList<>();

        String favoriteCoinListFromFile = readFavoriteList(this);

        //temporary data for prototype
        //String[] coinNamesAbv = {"BTC", "ETH", "XRP", "LTC", "ADA", "NEO", "XLM", "EOS", "XMR", "DASH", "MIOTA"};
        //String[] fullCoinName = {"Bitcoin", "Ethereum", "Ripple", "Litecoin", "Cardano", "NEO", "Stellar", "EOS", "Monero" , "Dash", "IOTA"};
		//
		coinNamesAbv=CoinNameResolver.getShortenedNameArray();
		fullCoinName=CoinNameResolver.getRealNameArray();
		//

        //this is where data can be pushed to cards
        for(int itemNum = 0; itemNum < fullCoinName.length; itemNum++)
        {
            //creates new object from coin_item.java
            /*
            if(itemNum == 0)
            {
                coin_item CItem = new coin_item(fullCoinName[itemNum],coinNamesAbv[itemNum], itemNum+900, 10, favoriteCoinListFromFile.contains(coinNamesAbv[itemNum]));
                CItem.setMoneyInvested(9219,10 );
                coinItem.add(CItem);
            }
            else if(itemNum == 2)
            {
                coin_item CItem = new coin_item(fullCoinName[itemNum],coinNamesAbv[itemNum], itemNum+900, 10, favoriteCoinListFromFile.contains(coinNamesAbv[itemNum]));
                CItem.setMoneyInvested(91360, 10);
                coinItem.add(CItem);
            }

            else if(itemNum == 3)
            {
                coin_item CItem = new coin_item(fullCoinName[itemNum],coinNamesAbv[itemNum], itemNum+900, 10, favoriteCoinListFromFile.contains(coinNamesAbv[itemNum]));
                CItem.setMoneyInvested(9136, 10);
                coinItem.add(CItem);
            }
            */
            //else{
                coin_item CItem = new coin_item(
                		fullCoinName[itemNum],
						coinNamesAbv[itemNum],
						itemNum+900,
						0,
						favoriteCoinListFromFile.contains(coinNamesAbv[itemNum])
				);
                coinItem.add(CItem);
            //}
        }

        //end of prototype block-------------------------------------------------------------------------------------------------------------------------
        List<coin_item> favoriteItem = new ArrayList<>();
        List<coin_item> nonfavoriteItem = new ArrayList<>();
        for(int coinItemIndex = 0; coinItemIndex < fullCoinName.length; coinItemIndex++)
        {
            if(coinItem.get(coinItemIndex).isCoinFavortieboolean() == true)
            {
                favoriteItem.add(coinItem.get(coinItemIndex));
            }
            else
            {
                nonfavoriteItem.add(coinItem.get(coinItemIndex));
            }
        }
        sortedCoinList.addAll(favoriteItem);
        sortedCoinList.addAll(nonfavoriteItem);

        //update data test thread
        //run(this);
		kill_event=new Semaphore(1);
		updater=new RecyclerThread();
		activated=true;
		updater.start();
		//Toast.makeText(coin_list.this, database.GetData("Ethereum").getCoinName(), Toast.LENGTH_LONG).show();
		mAdapter=new coinListAdapter(sortedCoinList,this);
		mRecyclerView.setAdapter(mAdapter);

	}
	protected void onStart()
	{
		super.onStart();
		Intent goal=new Intent(this,CurrencyService.class);
		boolean real;
		real=bindService(
				goal,
				hookup,
				Context.BIND_AUTO_CREATE
		);
	}
	protected void onDestroy()
	{
		super.onDestroy();
		activated=false;
		kill_event.acquireUninterruptibly(); // acts like an event
		unbindService(hookup);
		kill_event.release();
	}
	//
	private class RecyclerThread extends Thread
	{
		CurrencyService server;
		public void run()
		{
			int i;
			server=null;
			do
			{
				server=hookup.acquire();
			}
			while(server==null);
			// hook up has been done can now use the service
			kill_event.acquireUninterruptibly();
			while(activated)
			{
				try
				{
					runOnUiThread(new RecyclerRunnable());
					Thread.sleep(1000);
					// if the thread sleep count is to low the app will hang
				}
				catch(InterruptedException ie)
				{
				}
			}
			kill_event.release(); // not activated will kill
		}
		private class RecyclerRunnable implements Runnable
		{
			@Override
			public void run()
			{
				saveFavoriteCoinStringListToFile(sortedCoinList);
				//String favoriteCoinListFromFile = readFavoriteList(context);
				int item_index;
				for(item_index=0;item_index<fullCoinName.length;item_index++)
				{
					// add 3.5 to every coin in the list
					String name=sortedCoinList.get(item_index).getCoinNameAbv();
					sortedCoinList.get(item_index).setCoinPrice(
							//sortedCoinList.get(item_index).getCoinPrice()
							//+
							//3.5
							server.getLatest(name)
					);
					double queryCoinsValueHeld = Double.valueOf(database.GetData(sortedCoinList.get(item_index).getCoinName()).getCoinPrice());
//if(queryCoinsValueHeld == 0)
//{
					sortedCoinList.get(item_index).setMoneyInvested(queryCoinsValueHeld,Double.valueOf(database.GetData(sortedCoinList.get(item_index).getCoinName()).getCoinsHeld()) );
//}
                        int id = database.updateCoin(database.GetData(sortedCoinList.get(item_index).getCoinName()), 0, 0, (float)sortedCoinList.get(item_index).getCoinPrice());

//sortedCoinList.get(coinItemIndex).setMoneyInvested(Double.valueOf(database.GetData(sortedCoinList.get(coinItemIndex).getCoinName()).getCoinPrice()),  Double.valueOf(database.GetData(sortedCoinList.get(coinItemIndex).getCoinName()).getCoinsHeld()) );
					mAdapter.notifyDataSetChanged();
				}
			}
		}
	}

		/*
        public void run(final Context context)
        {
            new Thread()
            {
                public void run()
                {
                    for(int amountOfChanges = 0; amountOfChanges < 25; amountOfChanges++)
                    {
                        try{
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    saveFavoriteCoinStringListToFile(sortedCoinList);
                                    //String favoriteCoinListFromFile = readFavoriteList(context);
                                    for(int coinItemIndex = 0; coinItemIndex < 10; coinItemIndex++)
                                    {
                                        //add 3.5 to every coin in the list
                                        sortedCoinList.get(coinItemIndex).setCoinPrice(sortedCoinList.get(coinItemIndex).getCoinPrice() + 3.5);

                                        double queryCoinsValueHeld = Double.valueOf(database.GetData(sortedCoinList.get(coinItemIndex).getCoinName()).getCoinPrice());
                                        //if(queryCoinsValueHeld == 0)
                                        //{
                                            sortedCoinList.get(coinItemIndex).setMoneyInvested(queryCoinsValueHeld,Double.valueOf(database.GetData(sortedCoinList.get(coinItemIndex).getCoinName()).getCoinsHeld()) );
                                        //}
                                        //sortedCoinList.get(coinItemIndex).setMoneyInvested(Double.valueOf(database.GetData(sortedCoinList.get(coinItemIndex).getCoinName()).getCoinPrice()),  Double.valueOf(database.GetData(sortedCoinList.get(coinItemIndex).getCoinName()).getCoinsHeld()) );

                                    }
                                    mAdapter.notifyDataSetChanged();
                                }
                            });
                            Thread.sleep(3000);
                        }catch (InterruptedException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        }
        */



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.threedotmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch(id)
        {
            case R.id.setting:
                Intent mine = new Intent(coin_list.this, SettingsActivity.class);
                startActivity(mine);
                break;

            case R.id.Portfolio:
                Intent mine1 = new Intent(coin_list.this, PortfolioActivity.class);
                startActivity(mine1);
                break;

            case R.id.walletButton:

                String profitData = readProfitsToFile(coin_list.this);
                String delims = "[ ]+";
                String[] tokens = profitData.split(delims);

                final Dialog profitDialog = new Dialog(coin_list.this);
                profitDialog.setContentView(R.layout.profit_dialog_layout);
                profitDialog.setTitle("Profit");

                TextView profitText = (TextView) profitDialog.findViewById(R.id.profitDialogTextView);
                profitText.setText(String.format("%.3f", Double.valueOf(tokens[0])));

                TextView coinHeldDialog = (TextView) profitDialog.findViewById(R.id.coinHeldDialogText);
                coinHeldDialog.setText(String.format("%.3f", Double.valueOf(tokens[1])));

                profitDialog.show();

                break;


        }
        return true;
    }

    public String readFavoriteList(Context context)
    {
        String favoriteCoinListFromFile = "";

        try {

            InputStream inputStream = context.openFileInput("FavoriteCoinList.txt");

            if(inputStream != null)
            {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine() ) != null )
                {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                favoriteCoinListFromFile = stringBuilder.toString();
            }

        }
        catch (FileNotFoundException e)
        {
            Log.e("coin_list acctivity","FILE NOT FOUND: " + e.toString());
        }
        catch (IOException e)
        {
            Log.e("coin_list activity","cannot read file" + e.toString());
        }


        return favoriteCoinListFromFile;

    }
    public String readProfitsToFile(Context context)
    {
        String values = "";

        try {

            InputStream inputStream = context.openFileInput("profitData.txt");

            if(inputStream != null)
            {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine() ) != null )
                {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                values = stringBuilder.toString();
            }

        }
        catch (FileNotFoundException e)
        {
            Log.e("profit","FILE NOT FOUND: " + e.toString());
            return ("0 0 0");
        }
        catch (IOException e)
        {
            Log.e("profit","cannot read file" + e.toString());
        }


        return values;
    }

    public void saveFavoriteCoinStringListToFile(List<coin_item> coinItem)
    {
        String favoriteCoinStringList = "";

        for(int coinItemIndex = 0; coinItemIndex < fullCoinName.length; coinItemIndex++)
        {
            if(coinItem.get(coinItemIndex).isCoinFavortieboolean() == true)
            {
                favoriteCoinStringList = favoriteCoinStringList + " " + coinItem.get(coinItemIndex).getCoinNameAbv();
            }
        }

        try {
            OutputStreamWriter outputStreamFile = new OutputStreamWriter(this.openFileOutput("FavoriteCoinList.txt", this.MODE_PRIVATE));
            outputStreamFile.write(favoriteCoinStringList);
            outputStreamFile.close();

        }
        catch (IOException errorDescription)
        {
            Log.e("Exception", "File write failed: " + errorDescription.toString());
        }
    }



}



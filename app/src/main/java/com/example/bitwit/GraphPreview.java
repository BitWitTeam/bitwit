package com.example.bitwit;

import android.app.DatePickerDialog;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class GraphPreview extends SurfaceView implements
	SurfaceHolder.Callback,
	DatePickerDialog.OnDateSetListener,
	View.OnClickListener
{
	private class GraphDrawer extends TimerTask
	{
		private SurfaceView sv;
		public GraphDrawer(SurfaceView sv)
		{
			this.sv=sv;
		}
		@Override
		public void run()
		{
			// before canvas lock get checker value
			if(checker!=null)
			{
				was_checked=checker.isChecked();
			}
			// do the invalidation
			sv.postInvalidate(); // works now, invalidate by itself doesn't
		}
	}
	private TimerTask loader;
	private Timer ticker;
	public GraphPreview(Context context, AttributeSet attrib)
    {
        super(context, attrib);
		ticker=new Timer();
		loader=new GraphDrawer(this);
		ticker.schedule(loader,0,1000);
		//
        this.getHolder().addCallback(this);
        hookup=null;
		actual_name=null;
		// check box stuff
		checker=null;
		was_checked=false;
		// for drawing loading screen
		loading_text_dot_amount=0;
    }
    public void surfaceCreated(SurfaceHolder sh)
    {
        this.setWillNotDraw(false);
        //Toast.makeText(getContext(),"GraphCreated",Toast.LENGTH_SHORT).show();
    }
    public void surfaceDestroyed(SurfaceHolder sh)
    {
        //Toast.makeText(getContext(),"GraphDestroyed",Toast.LENGTH_SHORT).show();
		ticker.cancel(); // kills the timer
    }
    public void surfaceChanged(SurfaceHolder sh,int format,int width,int height)
    {
        //Toast.makeText(getContext(),"GraphChanged",Toast.LENGTH_SHORT).show();
        Canvas c=sh.lockCanvas();
        this.draw(c);
        sh.unlockCanvasAndPost(c);
    }
	private CurrencyServiceConnection hookup;
    public void setConnector(CurrencyServiceConnection hookup)
	{
		this.hookup=hookup;
	}
	private String actual_name;
	public void setCurrency(String actual_name)
	{
		this.actual_name=actual_name;
	}
	private CheckBox checker;
	private boolean was_checked;
	public void setCheckerWidget(CheckBox cb)
	{
		checker=cb;
	}
	private double getSmallest(double[] values)
	{
		int i;
		double smallest=values[0];
		for(i=0;i<values.length;i++)
		{
			if(values[i]<smallest)
			{
				smallest=values[i];
			}
		}
		return smallest;
	}
	private double getBiggest(double[] values)
	{
		int i;
		double biggest=values[0];
		for(i=0;i<values.length;i++)
		{
			if(values[i]>biggest)
			{
				biggest=values[i];
			}
		}
		return biggest;
	}
	private int[] transformPoints(int screen_height,double[] costs,int top_offset,int bottom_offset)
	{
		int[] values=new int[costs.length];
		double smallest,biggest;
		smallest=getSmallest(costs);
		biggest=getBiggest(costs);
		double delta=biggest-smallest;
		delta=(delta==0.0)?1.0:delta;
		for(int i=0;i<costs.length;i++)
		{
			double x;
			int y;
			// subtract by the smallest since that is the base
		//	x=costs[i]-smallest;
		//	x/=biggest;
			x=costs[i]-smallest;
			x/=delta;
			y=(int)(x*(bottom_offset-top_offset));
			values[i]=bottom_offset-(y+top_offset)+top_offset;
		//	int my_height=screen_height/4;
		//	values[i]=(values[i]<screen_height/2)?values[i]-my_height:values[i]+my_height;
		}
		return values;
	}
	private int loading_text_dot_amount;
    private final int DRAWING_POINTS=30;
    @Override
    protected void onDraw(Canvas canvas)
    {
        int width,height;
		CurrencyService cs;
		cs=hookup.acquire();
		if(cs==null||actual_name==null)
			return;
		width=canvas.getWidth();
		height=canvas.getHeight();
		double[] costs;
		double[] historical_prices;
		boolean has_historical_data;
		boolean got_historical_data;
		has_historical_data=CoinNameResolver.acquireSingleton().hasHistorical(actual_name);
		got_historical_data=false;
		historical_prices=null;
		if(was_checked)
		{
			local_infoer.setVisibility(View.VISIBLE);
			local_dater.setVisibility(View.VISIBLE);
		}
		else
		{
			local_infoer.setVisibility(View.INVISIBLE);
			local_dater.setVisibility(View.INVISIBLE);
		}
		if(was_checked)
		{
			// make a request to the currency service and then poll until it gets the data
			cs.requestHistoricalData(actual_name);
			historical_prices=cs.pollHistoricalData(actual_name);
			if(historical_prices!=null)
			{
				// if here got the data
				got_historical_data=true;
			}
			else
			{
				// figure out whether its even possible to get historical data for this currency
			}
		}
		if(got_historical_data&&historical_prices!=null)
		{
		//	double[] whatever={9025.73,8321.01,8246.57,8187.60,8430.93,8437.03,8195.34,8237.16,8051.95,6999.37};
			costs=historical_prices;
		}
		else // this will be the else part of the switch for "was_checked"
		{
			costs=cs.getMostRecentSlice(actual_name,DRAWING_POINTS);
		}
		if(was_checked&&has_historical_data)
		{
			if(got_historical_data)
			{
				// set costs to it
			}
			else
			{
				final String load_text="Data Loading";
				StringBuilder sb=new StringBuilder();
				// haven't gotten it yet so drawing loading in big letters
				canvas.drawColor(Color.parseColor("#383838"));
				Paint p=new Paint();
				p.setColor(Color.WHITE);
				float sentence_width=p.measureText(load_text+"...");
				float current_text_size=p.getTextSize();
				// scales by a multiple to fit text in the window by known previous size
				p.setTextSize(canvas.getWidth()/sentence_width*current_text_size);
				sb.append(load_text);
				for(int i=0;i<this.loading_text_dot_amount;i++)
				{
					sb.append('.');
				}
				canvas.drawText(sb.toString(),0,canvas.getHeight()/2,p);
				loading_text_dot_amount=(++loading_text_dot_amount)%4;
				return;
			}
		}
		int[] values;
        canvas.drawColor(Color.parseColor("#383838"));
        Paint p=new Paint();
        p.setColor(Color.BLACK);
        p.setStrokeWidth(
                width/100.0f // for now every hundred pixels is a point
        ); // best way to calculate stroke width?
        //SimilarRandoms sr=new SimilarRandoms(height); // no longer useful here could be basis for random elsewhere
		// draw the menu
		String menu_text;
		p.setTextSize(height/8.0f);
		float text_height=p.getTextSize();
		Paint.FontMetrics pfm=p.getFontMetrics();
		float text_extra;
		int top_offset,bottom_offset;
		//
		text_extra=text_height+pfm.descent;
		menu_text="High: "+Double.toString(getBiggest(costs));
		p.setColor(Color.LTGRAY);
		canvas.drawLine(0.0f,text_extra,width,text_extra,p);
		p.setColor(Color.WHITE);
		canvas.drawText(menu_text,0,text_height,p);
		top_offset=(int)text_extra;
		//
		text_extra=text_height+Math.abs(pfm.ascent);
		menu_text="Low: "+Double.toString(getSmallest(costs));
		p.setColor(Color.LTGRAY);
		canvas.drawLine(0.0f,height-text_extra,width,height-text_extra,p);
		p.setColor(Color.WHITE);
		canvas.drawText(menu_text,0,height-text_height,p);
		bottom_offset=(int)(height-text_extra);
		if(got_historical_data)
		{
			Paint.Align a=p.getTextAlign();
			p.setTextAlign(Paint.Align.RIGHT);
			canvas.drawText("(30 days of values from date)",width,height-text_height,p);
			p.setTextAlign(a);
		}
		//
		int distance=width/DRAWING_POINTS;
        values=transformPoints(height,costs,top_offset,bottom_offset);
		p.setColor(Color.BLACK);
		int previous_value=values[0];
        for(int line=1;line<values.length;line++) // draw for thirty values
        {
            int current_value;
			current_value=values[line];
            // the floats going into drawline are seen as integers
			if(current_value==previous_value)
				p.setColor(Color.BLUE);
			else if(current_value>previous_value)
				p.setColor(Color.RED);
			else
				p.setColor(Color.GREEN);
            canvas.drawLine(line*distance,previous_value,(line+1)*distance,current_value,p);
            previous_value=current_value;
        }
    }
    private TextView local_infoer;
    public void setTextInfoer(TextView infoer)
	{
		local_infoer=infoer;
	}
    private TextView local_dater;
    public void setTextDater(TextView dater)
	{
		local_dater=dater;
	}
	@Override
	public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
	{
		Calendar c=Calendar.getInstance();
		long current_epoch=System.currentTimeMillis();
		c.set(year,month,dayOfMonth);
		long since_epoch=c.getTimeInMillis();
		long initial_time=CoinNameResolver.acquireSingleton().getCreation(actual_name);
		if(since_epoch>current_epoch)
		{
			Toast.makeText(getContext(),"Can't set to a date in the future.",Toast.LENGTH_SHORT).show();
		}
		else if(initial_time>since_epoch)
		{
			Toast.makeText(getContext(),"Can't set date to before currency creation.",Toast.LENGTH_SHORT).show();
		}
		else
		{
			local_dater.setText(TimeUtility.build(since_epoch));
			// a valid day offset from now, need to notify the currency service
			CurrencyService cs;
			cs=hookup.acquire();
			if(cs==null||actual_name==null)
				return;
			int days_past;
			days_past=TimeUtility.daysBetween(current_epoch,since_epoch);
			cs.invalidateHistoricalDataTo(actual_name,days_past);
		}
	}
	private FragmentManager local_fm=null;
	public void setFM(FragmentManager fm)
	{
		local_fm=fm;
	}
	@Override
	public void onClick(View v)
	{
		DateDialogFragment ddf=new DateDialogFragment();
		ddf.usingListener(this);
		ddf.show(local_fm,"");
	}
}

package com.example.bitwit;
import java.util.concurrent.Semaphore;
import java.util.HashMap;
import java.util.Queue;
import java.util.ArrayDeque;
class CurrencyManager
// in the future will actually get values from
//  the net but right now is only a RNG
// Also in the future this would use multiple
//  arrays for currencies
{
	private Semaphore data_access;
	// all these variables below should only be accessed when the above mutex is signaled
	private boolean worker_thread_running;
	private HashMap<String,CurrencyList> markets;
	private Queue<String> update_list;
	public CurrencyManager(String[] currency_names)
	{
		// only one CurrencyList in use right now
		markets=new HashMap<String,CurrencyList>();
		update_list=new ArrayDeque<String>(); // use a Deque as a Queue
		for(String coin_name:currency_names)
		{
			// each coin gets a place to be looked up
			markets.put(coin_name,new CurrencyList());
			update_list.add(coin_name); // adding priority in the sequence they come in
		}
		// technically only a mutex since value has maximum of one
		data_access=new Semaphore(1);
		// would start a thread here
		worker_thread_running=true; // set before so don't have to use mutex here
		WorkerThread wt=new WorkerThread();
		wt.start();
	}
	public void destroy()
	// must be called to correctly deconstruct
	{
		data_access.acquireUninterruptibly(); // acquire mutex
		worker_thread_running=false; // instruct thread to kill self
		data_access.release(); // leave and object will have nothing left
	}
	public double getLatest(String currency_name)
	// should a string lookup be used or enumeration?
	{
		double value;
		// right now only testing out where mutex works
		//  make believe this is coming from the other thread
		// blocks until access or interrupted
		data_access.acquireUninterruptibly();
		CurrencyList cl;
		cl=markets.get(currency_name);
	//	if(cl==null) // complain?
		value=cl.getNewest();
		// definitely shouldn't be in the try
		//  should it be in the acquire since IE can happen
		//  at any time?
		data_access.release();
		return value;
	}
	public double[] getMostRecentSlice(String currency_name,int maximum)
	{
		double[] values;
		data_access.acquireUninterruptibly();
		CurrencyList cl;
		cl=markets.get(currency_name);
		values=cl.getRecentSlice(maximum);
		data_access.release();
		return values;
	}
	// should "close" be used? How to kill the thread?
	private class WorkerThread extends Thread
	{
		private boolean waiting_sign;
		public WorkerThread()
		{
			waiting_sign=false;
		}
		public void run()
		{
			// official way of killing a thread in java is put worker's code
			//  in a try with a loop and then when interrupted a flag is updated
			//  but what about releasing the mutex?
			// To do this without the interrupts instead during the mutex
			//  check a flag which has been updated, this flag depends upon
			//  calling the deconstructor of the containing class
			// this thread will also choose what is the best currency to load
			//  it will probably use a Queue to do this in choosing
			//  while also keeping track of the API time
			DataObtainer messenger=new DataObtainer();
		//	messenger.setUseFakeData(true);
			while(worker_thread_running) // the use of this variable is a problem
			// it should be inside the mutex since it is changed and set by the mutex
			{
			//	System.out.println("First Message");
				// retrieve from the queue what to update right now
				data_access.acquireUninterruptibly();
				String coin=update_list.remove(); // get the coin to update this mutex
				update_list.add(coin); // put the coin back at the beginning of the list
				CurrencyList cl=markets.get(coin); // make sure not null list?
				double price;
				try
				{
					price=messenger.deliver(coin); // can cause an exception
					// which means never get to adding the list
					cl.append(price); // use list to place newest value
				}
				catch(RuntimeException re)
				{
					waiting_sign=true;
				}
				finally
				{
					data_access.release();
				}
				try
				{
					//Thread.sleep(1000); // to make sure data is different give it some time
					Thread.sleep(10); // to make sure data is different give it some time
				}
				catch(InterruptedException ie)
				{
				}
				if(waiting_sign)
				{
					try
					{
						Thread.sleep(3000); // wait three seconds
						// was 3000
					}
					catch(InterruptedException ie)
					{
					}
					waiting_sign=false;
				}
			//	System.out.println("Second Message");
			}
		}
	}
}
package com.example.bitwit;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Log;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by juan on 3/7/2018.
 */

public class coinListAdapter extends RecyclerView.Adapter <RecyclerView.ViewHolder>{

    private List<coin_item> coinItem;
    private String favoriteCoinStringList = "";
    private Context context;

    //view holder generic non-owned view type
    class ViewHolder extends RecyclerView.ViewHolder{


        public TextView textViewCoinName;
        public TextView textViewCoinPrice;
        public TextView textViewcoinNameAbv;
        public LinearLayout linearLayout;
        public CheckBox favoriteCoinCheckBox;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewcoinNameAbv = (TextView) itemView.findViewById(R.id.CoinAbv);
            textViewCoinName = (TextView) itemView.findViewById(R.id.coinName);
            textViewCoinPrice = (TextView) itemView.findViewById(R.id.coinPrice);

            favoriteCoinCheckBox = (CheckBox)itemView.findViewById(R.id.coinLikeCheckBox);

            linearLayout=(LinearLayout) itemView.findViewById(R.id.coinCardLayout);
        }
    }

    //view holder Owned view type
    class ViewHolder2 extends RecyclerView.ViewHolder {


        public TextView textViewCoinName;
        public TextView textViewCoinPrice;
        public TextView textViewCoinValueHeld;
        public TextView textViewcoinNameAbv;
        public TextView textViewValueChange;
        public LinearLayout linearLayout;
        public CheckBox favoriteCoinCheckBox;


        public ViewHolder2(View itemView)
        {
            super(itemView);

            textViewcoinNameAbv = (TextView) itemView.findViewById(R.id.CoinAbv);
            textViewCoinName = (TextView) itemView.findViewById(R.id.coinName);
            textViewCoinPrice = (TextView) itemView.findViewById(R.id.coinPrice);
            textViewCoinValueHeld = (TextView) itemView.findViewById(R.id.coinHeldValue);
            textViewValueChange = (TextView) itemView.findViewById(R.id.valueChange);

            favoriteCoinCheckBox = (CheckBox)itemView.findViewById(R.id.coinLikeCheckBox);

            linearLayout=(LinearLayout) itemView.findViewById(R.id.coinCardLayout);
        }
    }

    @Override
    public int getItemViewType(int position) {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous

        if(coinItem.get(position).getCoinsHeld() != 0)
        {
            return 2;
        }
        else
        {
            return 0;
        }

    }

    public coinListAdapter(List<coin_item> coinItem, Context context) {
        this.coinItem = coinItem;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        //View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.coin_item, parent, false);
        //return new ViewHolder(v);
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater li=LayoutInflater.from(viewGroup.getContext());

        switch(viewType)
        {
            case 0:
                View v=li.inflate(R.layout.coin_item,viewGroup,false);
                viewHolder = new ViewHolder(v);
                return viewHolder;
                //break;

            case 2:
                View v2=li.inflate(R.layout.coin_item_owned,viewGroup,false);
                viewHolder = new ViewHolder2(v2);
                return viewHolder;
               // break;

            default:
                View v3=li.inflate(R.layout.coin_item,viewGroup,false);
                viewHolder = new ViewHolder2(v3);
                return viewHolder;
                //break;
            //break;
        }

    }

    // could use something like "String.format("%+.6f",number);" instead
	DecimalFormat visible_display_formatter=new DecimalFormat("#.######");
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final coin_item cItem = coinItem.get(position);

        // this value will always be used so set it up
        final String current_coin_price=visible_display_formatter.format(cItem.getCoinPrice());

        switch (holder.getItemViewType())
        {
            //generic coin version
            case 0:
                final ViewHolder holder1 = (ViewHolder) holder;
                holder1.textViewCoinName.setText(cItem.getCoinName());
                //holder1.textViewCoinPrice.setText( String.valueOf(cItem.getCoinPrice()) );
				holder1.textViewCoinPrice.setText(current_coin_price);
                holder1.textViewcoinNameAbv.setText(cItem.getCoinNameAbv());
                //prevents checkbox issues from notifyDataUpdate function
                holder1.favoriteCoinCheckBox.setOnCheckedChangeListener(null);
                holder1.favoriteCoinCheckBox.setChecked(cItem.isCoinFavortieboolean());

                if( cItem.isCoinFavortieboolean() == true)
                {
                    favoriteCoinStringList = favoriteCoinStringList + " " + cItem.getCoinNameAbv();
                }

                //add favorited item to list of favorites
                holder1.favoriteCoinCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean coinFavoriteStatus) {

                        if(coinFavoriteStatus == true)
                        {
                            cItem.setCoinFavortieboolean(coinFavoriteStatus);
                            //Toast.makeText(context, "FAV: " + String.valueOf(cItem.isCoinFavortieboolean()), Toast.LENGTH_LONG).show();
                            favoriteCoinStringList = favoriteCoinStringList + " " + cItem.getCoinNameAbv();
                        }
                        if(coinFavoriteStatus == false)
                        {
                            cItem.setCoinFavortieboolean(coinFavoriteStatus);
                            //Toast.makeText(context, "FAV: " + String.valueOf(cItem.isCoinFavortieboolean()), Toast.LENGTH_LONG).show();
                            favoriteCoinStringList = favoriteCoinStringList.replace(cItem.getCoinNameAbv(), " ");
                        }
                    }
                });

                holder1.linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Toast.makeText(context, "You clicked "+ cItem.getCoinName() + "with price "+ cItem.getCoinPrice(), Toast.LENGTH_LONG).show();
                        final Intent coinIntent = new Intent(context, BuyPage.class);
                        Bundle coinInformation = new Bundle();
                        coinInformation.putString("coinName", cItem.getCoinName());
                        //coinInformation.putString("coinPrice", String.valueOf(cItem.getCoinPrice()) );
						// so this value here is only affecting the BuyPage displayed value
						//  but not the coinlist one
						coinInformation.putString("coinPrice",current_coin_price);
                        coinIntent.putExtras(coinInformation);
                        saveFavoriteCoinStringListToFile();//test
                        context.startActivity(coinIntent);
                    }
                });
                break;

            //coin_owned version
            case 2:
            	// using a different pattern here
				double investment_cost=cItem.getMoneyInvested();
            	double stock_value=cItem.getCoinPrice()*cItem.getCoinsHeld();
				String stock_text=visible_display_formatter.format(stock_value);
				double profts_value=stock_value-investment_cost;
				String profits_text=visible_display_formatter.format(profts_value);

                ViewHolder2 holder2 = (ViewHolder2) holder;
                holder2.textViewCoinName.setText(cItem.getCoinName());
                //holder2.textViewCoinPrice.setText( String.valueOf(cItem.getCoinPrice()) );
				// so this value here is only affecting the BuyPage displayed value
				//  but not the coinlist one
				holder2.textViewCoinPrice.setText(current_coin_price);
                //holder2.textViewCoinValueHeld.setText( String.valueOf( roundValue(cItem.getCoinPrice() * cItem.getCoinsHeld()) ) ) ;
				holder2.textViewCoinValueHeld.setText(stock_text);
                holder2.textViewcoinNameAbv.setText(cItem.getCoinNameAbv());

                //holder2.textViewValueChange.setText( String.valueOf (  roundValue( ( cItem.getCoinPrice() * cItem.getCoinsHeld() ) - cItem.getMoneyInvested() ) )  );
				// this now handles all the cases except when exactly zero
				holder2.textViewValueChange.setText(profits_text);

                //color changes to card background and numeric values
                //if(  cItem.getCoinPrice() * cItem.getCoinsHeld() > cItem.getMoneyInvested() )
				if(stock_value>investment_cost)
                {
                    holder2.textViewValueChange.setText( "+" + profits_text );
                    holder2.itemView.setBackgroundResource(R.color.positive);
                    holder2.textViewValueChange.setTextColor(Color.parseColor("#558B2F"));
                }
                //else if( cItem.getCoinPrice() * cItem.getCoinsHeld() < cItem.getMoneyInvested())
				else if(stock_value<investment_cost)
                {
                    holder2.textViewValueChange.setText( profits_text  );
                    holder2.itemView.setBackgroundResource(R.color.negative);
                    holder2.textViewValueChange.setTextColor(Color.RED);
                }
                else
                {
                    holder2.textViewValueChange.setText( profits_text  );
					holder2.textViewValueChange.setText("0");
                    holder2.itemView.setBackgroundResource(R.color.colorPrimary);
                    holder2.textViewValueChange.setTextColor(Color.BLUE);
                }


                //changes checkbox to appropriate object value
                holder2.favoriteCoinCheckBox.setChecked(cItem.isCoinFavortieboolean());

                //if checkbox is set , coinAbv added to string for favoriteCoin file
                if( cItem.isCoinFavortieboolean() == true)
                {
                    favoriteCoinStringList = favoriteCoinStringList + " " + cItem.getCoinNameAbv();
                }

                //add favorited item to list of favorites
                holder2.favoriteCoinCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean coinFavoriteStatus) {

                        if(coinFavoriteStatus == true)
                        {
                            cItem.setCoinFavortieboolean(coinFavoriteStatus);
                            //Toast.makeText(context, "FAV: " + String.valueOf(cItem.isCoinFavortieboolean()), Toast.LENGTH_LONG).show();
                            favoriteCoinStringList = favoriteCoinStringList + " " + cItem.getCoinNameAbv();
                        }
                        if(coinFavoriteStatus == false)
                        {
                            cItem.setCoinFavortieboolean(coinFavoriteStatus);
                            //Toast.makeText(context, "FAV: " + String.valueOf(cItem.isCoinFavortieboolean()), Toast.LENGTH_LONG).show();
                            favoriteCoinStringList = favoriteCoinStringList.replace(cItem.getCoinNameAbv(), " ");
                        }

                    }
                });

                holder2.linearLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Toast.makeText(context, "You clicked "+ cItem.getCoinName() + "with price "+ cItem.getCoinPrice(), Toast.LENGTH_LONG).show();
                        final Intent coinIntent = new Intent(context, BuyPage.class);
                        Bundle coinInformation = new Bundle();
                        coinInformation.putString("coinName", cItem.getCoinName());
                        //coinInformation.putString("coinPrice", String.valueOf(cItem.getCoinPrice()) );
						coinInformation.putString("coinPrice",current_coin_price);
                        coinIntent.putExtras(coinInformation);
                        saveFavoriteCoinStringListToFile();//test
                        context.startActivity(coinIntent);
                    }
                });
                break;
        }


    }

    @Override
    public int getItemCount()
    {

        return coinItem.size();
    }

    public void saveFavoriteCoinStringListToFile()
    {
        try {
            OutputStreamWriter outputStreamFile = new OutputStreamWriter(context.openFileOutput("FavoriteCoinList.txt", context.MODE_PRIVATE));
            outputStreamFile.write(favoriteCoinStringList);
            outputStreamFile.close();

        }
        catch (IOException errorDescription)
        {
            Log.e("Exception", "File write failed: " + errorDescription.toString());
        }
    }

    /*
    double roundValue(double value)
    {
        DecimalFormat threeDec = new DecimalFormat(
        		//"#.##"
				"#.######"
		);
        return Double.valueOf(threeDec.format(value));
    }
    */


}

package com.example.bitwit;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.SQLOutput;


public class BuyPage extends AppCompatActivity {

    Database database = new Database(this);
    boolean buyBoolean= true;
    boolean USDspinnerOption = true;

    float coinPrice = 0;
    float totalPurchaseAmount = 0;

    TextView totalValueofPotentialPurchase;
    String coinName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_page);

        getWindow().setNavigationBarColor(Color.parseColor("#383838"));

        Intent recievedCoinIntent = getIntent();
        Bundle recivedCoinBundle = recievedCoinIntent.getExtras();
        coinName = recivedCoinBundle.getString("coinName");
        coinPrice = Float.parseFloat(recivedCoinBundle.getString("coinPrice"));

        final Button buyButton = (Button) findViewById(R.id.buyButton);
        buyButton.setBackgroundResource(R.drawable.green_gradient);


        // set recieved coin name to textview
        final TextView coinNameTextViewStringHolder = (TextView)findViewById(R.id.coinNameHeaderTextView);
        coinNameTextViewStringHolder.setText(coinName);

        //set recieved coin price to textview
        final TextView coinPriceTextViewStringHolder = (TextView)findViewById(R.id.coinPriceTextView);
        coinPriceTextViewStringHolder.setText(String.valueOf(coinPrice));

        final TextView provriousOwnedCoinsTextview = (TextView)findViewById(R.id.previousOwnedCoins);
        provriousOwnedCoinsTextview.setText("Coins: " + database.GetData(coinName).getCoinsHeld());

        final TextView provriousOwnedValueTextview = (TextView)findViewById(R.id.previousOwnedValue);
        provriousOwnedValueTextview.setText("Investment: " + database.GetData(coinName).getCoinPrice());

        final ImageView topBar = findViewById(R.id.imageView);
        topBar.setBackgroundResource(R.drawable.green_gradient);

        final ImageView topDivider = findViewById(R.id.topDividorImageView);
        topDivider.setBackgroundResource(R.drawable.green_gradient);


        //return position of buy sell switch
        Switch buySellSwitch = (Switch) findViewById(R.id.buySellSwitch);
        buySellSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Button buyButtonText = (Button)findViewById(R.id.buyButton);
                if (isChecked) {
                    topBar.setBackgroundResource(R.drawable.red_gradient);
                    topDivider.setBackgroundResource(R.drawable.red_gradient);


                    buyButton.setBackgroundResource(R.drawable.red_gradient);


                    buyButtonText.setText("SELL");
                    buyBoolean = false;
                    // The toggle is enabled
                } else {
                    topBar.setBackgroundResource(R.drawable.green_gradient);
                    topDivider.setBackgroundResource(R.drawable.green_gradient);


                    buyButton.setBackgroundResource(R.drawable.green_gradient);


                    buyButtonText.setText("BUY");
                    buyBoolean=true;
                    // The toggle is disabled
                }
            }
        });

        //dynamicaly change total based on user input
        totalValueofPotentialPurchase = (TextView)findViewById(R.id.totalTextView);
        final EditText userEnteredValueEditText = (EditText) findViewById(R.id.userEnteredValue);
        userEnteredValueEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                totalValueofPotentialPurchase.setText("- E -");
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                String value  = String.valueOf(userEnteredValueEditText.getText());
                if(value.matches("^\\d*\\.?\\d+$")) {

                    if(USDspinnerOption == true)
                    {
                        totalPurchaseAmount = Float.parseFloat(value);
                    }
                    else
                    {
                        totalPurchaseAmount = Float.parseFloat(value) * coinPrice;
                    }

                    totalValueofPotentialPurchase.setText(String.valueOf(totalPurchaseAmount));
                }
                else
                {
                    totalValueofPotentialPurchase.setText("0");
                }
            }
        });

        //return poisiton of buy with spinner
        Spinner currecnySpinner = (Spinner) findViewById(R.id.buySellOptionSpinner);

        String [] spinItem = new String[]{"USD", "Coin"};

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>( this, R.layout.spinner_white_text, spinItem);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_white_text);

        currecnySpinner.setAdapter(spinnerArrayAdapter);

        currecnySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                //erase previous data in total section
                //totalPurchaseAmount = 0;
                //totalValueofPotentialPurchase.setText(String.valueOf(totalPurchaseAmount));
                totalValueofPotentialPurchase.setText("- E -");
                userEnteredValueEditText.setText("");

                String selectedItem = parent.getItemAtPosition(position).toString(); //this is your selected item
                if(selectedItem.equals("USD") )
                {
                    //Toast.makeText(BuyPage.this, "USD", Toast.LENGTH_LONG).show();
                    USDspinnerOption = true;
                }
                else
                {
                    //Toast.makeText(BuyPage.this, "coin", Toast.LENGTH_LONG).show();
                    USDspinnerOption=false;
                }
            }
            public void onNothingSelected(AdapterView<?> parent)
            {
                totalValueofPotentialPurchase.setText("- E -");
                //nothing selcted from spinner
            }
        });

        //if coinPrice is 0, disable buy button so there are no database issues
        if(coinPrice <= 0.0)
        {
            buyButton.setEnabled(false);

        }


        // buy/sell buttom triggered
        buyButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if(totalPurchaseAmount != 0) {


                    //buying
                    if (buyBoolean) {
                        try {
                            //buy: update existing record in database
                            if(Integer.valueOf(database.GetData(coinName).getCoinName()) == 0)
                            {
                                //Toast.makeText(BuyPage.this, "ADD", Toast.LENGTH_LONG).show();
                                long id = database.Add(coinName, String.valueOf(totalPurchaseAmount), String.valueOf(totalPurchaseAmount / coinPrice), String.valueOf(coinPrice));
                                writeProfitToFile(0, totalPurchaseAmount / coinPrice, totalPurchaseAmount , BuyPage.this);
                            }
                            else
                            {
                                int id = database.updateCoin(database.GetData(coinName), totalPurchaseAmount, totalPurchaseAmount / coinPrice, coinPrice);
                                writeProfitToFile(0, totalPurchaseAmount / coinPrice, totalPurchaseAmount , BuyPage.this);
                            }

                            //int id = database.updateCoin(database.GetData(coinName), totalPurchaseAmount, totalPurchaseAmount / coinPrice);
                        } catch (Exception s) {
                            //buy: create new record in database
                            //Toast.makeText(BuyPage.this, "ADD2", Toast.LENGTH_LONG).show();
                            //long id = database.Add(coinName, String.valueOf(totalPurchaseAmount), String.valueOf(totalPurchaseAmount / coinPrice));
                            int id = database.updateCoin(database.GetData(coinName), totalPurchaseAmount, totalPurchaseAmount / coinPrice, coinPrice);
                            writeProfitToFile(0, totalPurchaseAmount / coinPrice, totalPurchaseAmount , BuyPage.this);
                        }

                    }

                    //selling
                    else {
                        try {
                            if ((Float.parseFloat(database.GetData(coinName).getCoinPrice()) - totalPurchaseAmount) >= 0) {

                                //selling: when result is expected to be zero...prevents weird issue where coins held field would turn to random value
                                if ((Float.parseFloat(database.GetData(coinName).getCoinPrice()) - totalPurchaseAmount) == 0) {

                                    writeProfitToFile((coinPrice - oldCoinValue()) * (totalPurchaseAmount / coinPrice), -(totalPurchaseAmount / coinPrice), 0 , BuyPage.this);
                                    int id = database.updateCoin(database.GetData(coinName), -(Float.valueOf(database.GetData(coinName).getCoinPrice())), -(Float.valueOf(database.GetData(coinName).getCoinsHeld())), coinPrice);
                                    //writeProfitToFile((coinPrice - oldCoinValue()) * (totalPurchaseAmount / coinPrice), -(totalPurchaseAmount / coinPrice), 0 , BuyPage.this);
                                }

                                //selling: update specified coin record in database
                                //normal sell that wont result in zero values in coin record in database
                                else {

                                    writeProfitToFile((coinPrice - oldCoinValue()) * (totalPurchaseAmount / coinPrice), -(totalPurchaseAmount / coinPrice), 0 , BuyPage.this);
                                    int id = database.updateCoin(database.GetData(coinName), -totalPurchaseAmount, (totalPurchaseAmount * -1) / coinPrice, coinPrice);

                                    if (id < 0) ;
                                }

                            }

                            //trying to sell coins owner does not poses
                            else {
                                Toast.makeText(BuyPage.this, "Not Enough Funds", Toast.LENGTH_LONG).show();
                            }

                            //int id = database.updateCoin(database.GetData(coinName), -totalPurchaseAmount);
                            //if(id < 0);
                            //TODO Not enough funds, nothing was updated, print message to the user
                        } catch (Exception s) {
                        }

                    }

                    userEnteredValueEditText.setText("");

                    provriousOwnedValueTextview.setText("Investment: " + database.GetData(coinName).getCoinPrice());
                }
                provriousOwnedCoinsTextview.setText("Coins: " + database.GetData(coinName).getCoinsHeld());
                //Toast.makeText(BuyPage.this, " Name: " + database.GetData(coinName).getCoinName() + " Value held: " + database.GetData(coinName).getCoinPrice() + " coins: " + database.GetData(coinName).getCoinsHeld(), Toast.LENGTH_LONG).show();

            }
        });

		// get access to the SurfaceView
		String ticker_name;
		ticker_name=CoinNameResolver.acquireSingleton().getShortenedFromFull(coinName);
        GraphPreview gp=findViewById(R.id.graphPreview);
        hookup=new CurrencyServiceConnection();
        CoinNameResolver cnr=new CoinNameResolver();
        gp.setConnector(hookup);
        gp.setCurrency(ticker_name);
		// gain access to the check box that shows historical data
        CheckBox cb=(CheckBox)findViewById(R.id.checkBox); // wasn't able to get it inside
		// date text
		TextView dateText=(TextView)findViewById(R.id.clickableDialoger);
		dateText.setText(TimeUtility.build(System.currentTimeMillis())); // to today's date
		gp.setFM(getSupportFragmentManager());
		gp.setTextDater(dateText);
		dateText.setOnClickListener(gp);
		dateText.setVisibility(View.INVISIBLE);
		TextView empty=(TextView)findViewById(R.id.staticTextView);
		empty.setVisibility(View.INVISIBLE);
		gp.setTextInfoer(empty);
		/*
		dateText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogFragment df=new DialogFragment();
			//	df.show(getSupportFragmentManager()," date picker");
			}
		});
		*/
		dateText.setPaintFlags(dateText.getPaintFlags()|Paint.UNDERLINE_TEXT_FLAG);
		// only allow the widget to be drawn if the coin can get historical data
		if(!CoinNameResolver.acquireSingleton().hasHistorical(ticker_name))
		{
			cb.setVisibility(View.INVISIBLE);
			// this would also be where the MACD checkbox could be made invisible
			//  since the only way to calculate MACD would be using historical data
		}
		else
		{
			gp.setCheckerWidget(cb);
		}
        //cb.setEnabled(false); // doesn't change the drawing
        //sv.draw(new MyCanvas());
    }
    private CurrencyServiceConnection hookup;
    protected void onStart()
    {
        super.onStart();
        Intent goal=new Intent(this,CurrencyService.class);
        boolean real;
        real=bindService(
                goal,
                hookup,
                Context.BIND_AUTO_CREATE
        );
    }
    protected void onDestroy()
    {
        super.onDestroy();
        unbindService(hookup);
    }
    //
    /*
    private class MyCanvas extends Canvas
    {
        public MyCanvas()
        {
            this.drawLine(0.0f,0.0f,2.0f,2.0f,new Paint());
        }
    }
    */

    public double oldCoinValue()
    {
        double moneySpent = Double.valueOf(database.GetData(coinName).getCoinPrice());
        double coinsHeld = Double. valueOf(database.GetData(coinName).getCoinsHeld());

        return (moneySpent/coinsHeld);
    }

    public String readProfitsToFile(Context context)
    {
        String values = "";

        try {

            InputStream inputStream = context.openFileInput("profitData.txt");

            if(inputStream != null)
            {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine() ) != null )
                {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                values = stringBuilder.toString();
            }

        }
        catch (FileNotFoundException e)
        {
            Log.e("profit","FILE NOT FOUND: " + e.toString());
            return ("0 0 0");
        }
        catch (IOException e)
        {
            Log.e("profit","cannot read file" + e.toString());
        }


        return values;
    }

    public void writeProfitToFile(double profitChange, double coinHeldChange, double moneySpentChange, Context context)
    {
        String newProfitWallet = "";

        String previousData = readProfitsToFile(context);
        String delims = "[ ]+";
        String[] tokens = previousData.split(delims);

        double prevProfit = Double.valueOf(tokens[0]);
        double prevCoinHeld = Double.valueOf(tokens[1]);
        double prevSpent = Double.valueOf(tokens[2]);

        String newProfit = String.valueOf(prevProfit + profitChange);
        String newCoinHeld = String.valueOf(prevCoinHeld + coinHeldChange);
        String newSpent = String.valueOf(prevSpent + moneySpentChange);

        newProfitWallet = newProfit + " " + newCoinHeld + " " + newSpent;


        try {
            OutputStreamWriter outputStreamFile = new OutputStreamWriter(this.openFileOutput("profitData.txt", this.MODE_PRIVATE));
            outputStreamFile.write(newProfitWallet);
            outputStreamFile.close();

        }
        catch (IOException errorDescription)
        {
            Log.e("Exception", "File write failed: " + errorDescription.toString());
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.buy_page_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.walletButton) {
            //Toast.makeText(BuyPage.this, "yesssss", Toast.LENGTH_LONG).show();
            String profitData = readProfitsToFile(BuyPage.this);
            String delims = "[ ]+";
            String[] tokens = profitData.split(delims);

            final Dialog profitDialog = new Dialog(BuyPage.this);
            profitDialog.setContentView(R.layout.profit_dialog_layout);
            profitDialog.setTitle("Profit");

            TextView profitText = (TextView) profitDialog.findViewById(R.id.profitDialogTextView);
            profitText.setText(String.format("%.3f", Double.valueOf(tokens[0])));

            TextView coinHeldDialog = (TextView) profitDialog.findViewById(R.id.coinHeldDialogText);
            coinHeldDialog.setText(String.format("%.3f", Double.valueOf(tokens[1])));

            profitDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }
}

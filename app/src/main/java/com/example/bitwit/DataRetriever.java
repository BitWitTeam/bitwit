package com.example.bitwit;

public class DataRetriever {

    private String coinName;
    private String coinPrice;
    private String coinsHeld;
    private String marketValue;

    public DataRetriever(String coinName, String coinPrice, String coinsHeld, String marketValue)
    {
        this.coinName = coinName;
        this.coinPrice = coinPrice;
        this.coinsHeld = coinsHeld;
        this.marketValue = marketValue;
    }

    public String getCoinName() {
        return coinName;
    }
    public String getCoinPrice() { return coinPrice; }
    public String getCoinsHeld() {
        return coinsHeld;
    }
    public String getMarketValue() { return marketValue; }
}